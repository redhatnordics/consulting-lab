FROM registry.fedoraproject.org/fedora

RUN dnf update -y && dnf install ansible openssh-clients python3-netaddr python3-libvirt python3-jinja2 PyYAML -y 

VOLUME /playbooks

WORKDIR /playbooks
