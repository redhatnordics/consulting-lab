# consulting-lab

A repository containing collection of resources used by consultants in their lab environments.  

RH Consultants are often faced with the situation where they need to test and lab with quite extensive scenarios that requires a customer like environment and should interact with many different components.
Things like:
- Locked down networks and proxies
- High Availability and 
- IdM/LDAP authentication
- Doing things at scale

While most of the stuff you find in here can be used adhoc, there is also an idea of spinning up a complete lab in short amount of time.  
The lab is based around the idea that you have a very _large_ host that will act as your hypervisor, firewall, and gateway for the virtual networks.  
As of right now this is performed with libvirt and iptables and configured using multiple Ansible roles in combination.  

The goal is for you to be able to describe the labenvironment in a configuration file and press the button.   
For the automation we use ansible. We model the environment using ansible variables.    
Since it's a lot to configure we try to keep the defaults sensible and plenty.  

You find more instructions on how to create your own lab below.

**NOTE: This is not yet stable and rapid changes are to be expected at the moment**

## Global variables

There are some variables that's global and should be used by all roles where applicable.

| variable name | default | description                                                                                                                                                                     |
| ------------- | ------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| use_cdnmirror | false   | if set to true, VMs should use the CDN mirror VM for all rpm content instead of registering to the CDN with subscription-manager, if set to true repo_server needs to be define |
| repo_server   | null    | the IP of the CDN mirror, since DNS might not be up yet it's important that this is the IP of the cdnmirror VM                                                                  |

## Roles in this repository

| role                                                    | description                                                                            |
| ------------------------------------------------------- | -------------------------------------------------------------------------------------- |
| [cdnmirror](/roles/cdnmirror)                           | sets up a simple CDN mirror on a VM                                                    |
| [hetzner-post-provision](/roles/hetzner-post-provision) | role used for post provision tasks                                                     |
| [hetzner-provision](/roles/hetzner-provision)           | initial provision tasks when hetzner machine is in rescue mode                         |
| [knockd](/roles/knockd)                                 | configure port knocking                                                                |
| [libvirt](/roles/libvirt)                               | installs and configures libvirt                                                        |
| [libvirt-network](/roles/libvirt-network/)              | creates the libvirt networks from the `libvirt_networks:` defined in inventory         |
| [libvirt-iptables](/roles/libvirt-iptables)             | role to configure routing between networks created with libvirt_network                |
| [mdadm-sync](/roles/mdadm-sync)                         | pause and start mdadm RAID sync                                                        |
| [motd](/roles/motd)                                     | sets a motd on the target machine                                                      |
| [openvpn](/roles/openvpn)                               | sets up a static openvpn server                                                        |
| [reboot](/roles/reboot)                                 | reboots the target machine and waits for it come back                                  |
| [rhel-iso-download](/roles/rhel-iso-download)           | downloads images from redhat CDN. requires active subscription (see `subman`)          |
| [subman](/roles/subman)                                 | register and attaches subscription if not present                                      |
| [users](/roles/users)                                   | creates local users with authorized_keys and sudo                                      |
| [squid](/roles/squid)                                   | install a squid proxy                                                                  |
| [vm-template](/roles/vm-template)                       | creates an up-to-date rhel7.6 template image. deps: `subman` and `rhel-iso-download`   |
| [vm-create](/roles/vm-create)(DEPRECATED)               | creates VM's acord. to spec. Supports static ip and extra disks. deps: `vm-template`   |
| [reposync](/roles/reposync)(DEPRECATED)                 | creates a rhel7 repomirror on the labhost. requires active subscription (see `subman`) |

# Get Started

Please see the [documentation](https://consulting-lab.readthedocs.io/en/latest/Getting-Started/requirements/) for more details.

