from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from ansible.errors import AnsibleError
from ansible.module_utils.six import string_types
from ansible.plugins.action import ActionBase
from ansible.parsing.utils.addresses import parse_address
from ansible.utils.display import Display
import ipaddress

display = Display()

'''

This action_plugin/module will basically invoke the iptables module
It perform some checks to conform with how we want to
do port openings in consulting lab.

i.e. the port opening is always applied to a "to-<network>" chain.


network: mandatory
port: mandatory
protocol: (default tcp)
src: from where the traffic originates from.
dst: the destination of the traffic.  

To allow host A on network NetA
to reach Host B on network NetB on port 53:

lab_port:
    network: NetB
    port: 53
    proto: tcp
    dst: 10.170.30.4 (Host B's ipaddress)
    src: 172.30.0.4  (Host A's ipaddress)

'''

class ActionModule(ActionBase):


    def run(self, tmp=None, task_vars=None):
        result = super(ActionModule, self).run(tmp, task_vars)
        self.task_vars = task_vars
        module_args = self._task.args.copy()

        try: 
            network = module_args['network']
        except KeyError:
            raise AnsibleError('No network specified. The lab_port requires a network in order to add it to the correct to-<network> chain.')
        # get the inventory networks and check that they are aligned
        inv_networks = task_vars.get('networks')
        if not any(inv_network['name'] == network for inv_network in inv_networks):
            raise AnsibleError('The specified network %s is not in the list of available networks. Available networks are: %s' % (network, [inv_network['name'] for inv_network in inv_networks]))

        try:
            port= module_args['port']
        except KeyError:
            raise AnsibleError('No port number given.')
        try:
            port = int(port)
            if port > 65535:
                raise ValueError
        except ValueError:
            AnsibleError('port: must be a valid number port number between 1-65535')

        try:
            protocol = module_args['proto'].lower()
        except KeyError:
            protocol = "tcp"

        if "tcp" not in protocol and "udp" not in protocol:
            raise AnsibleError('protocol needs to be either "tcp" or "udp"')

        try:
            src = module_args['src']
            if src == '':
                src = "any"
            if src != 'any':
                try:
                    ipaddress.ip_address(src)
                except ValueError:
                    try:
                        ipaddress.ip_network(src)
                    except ValueError:
                        raise AnsibleError('src: needs to be either a valid IP, a valid ip subnet or "any". "%s" was given' % src)
        except KeyError:
            src = 'any'

        try:
            dst = module_args['dst']
            if dst == '':
                dst = "any"
            if dst != 'any':
                try:
                    ipaddress.ip_address(dst)
                except ValueError:
                    try:
                        ipaddress.ip_network(dst)
                    except ValueError:
                        raise AnsibleError('dst: needs to be either a valid IP, a valid ip subnet or "any". "%s" was given' % dst)
        except KeyError:
            dst = 'any'

        # validate that the destination ip is within the specified network subnet.
        # it doesn't make sense to use a dst for an ip that the -o network doesn't use.
        if dst != 'any':
            for inv_network in inv_networks:
                if network in inv_network['name']:
                    if dst is not None and not ipaddress.ip_address(dst) in ipaddress.ip_network(inv_network['subnet']):
                        raise AnsibleError("The dst: ip '%s' is not within network %s's subnet: %s " (dst,network,inv_network['subnet']))

#        if src is 'any' and dst is 'any': 
#            raise AnsibleError('Neither src: or dst: specified')

        try:
            comment = module_args['comment']
        except KeyError:
            comment = ''

        if comment == '':
            # No comment given. Create one out of the info we got
            comment = "Allow *%s* to access *%s* on port %s/%s" % (src,dst,port,protocol)

        iptables_args=dict()
        # Always insert at the top of the to-<network> chain
        iptables_args['action'] = "insert"
        iptables_args['rule_num'] = "1"
        # We only do ACCEPT with this module
        iptables_args['jump'] = "ACCEPT"
        iptables_args['chain'] = "to-" + str(network)
        iptables_args['protocol'] = protocol
        iptables_args['destination_port'] = port
        iptables_args['comment'] = comment

        # if src is any that means we are not going to add a --src in the iptables command.
        # the traffic may originate from anywhere and it should be forwarded/allowed.
        if src is not None and src != 'any' and src != '':
            iptables_args['source'] = src
        if src is not None and dst != 'any' and dst != '':
            iptables_args['destination'] = dst


        #raise AnsibleError('did we get here before module')
        # run the iptables module with the values we've compiled
        iptables_port_return = self._execute_module(module_name='iptables',
                                    module_args=iptables_args,
                                    task_vars=self.task_vars,
                                    tmp=tmp)
        return iptables_port_return
