from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from ansible.errors import AnsibleError
from ansible.module_utils.six import string_types
from ansible.plugins.action import ActionBase
from ansible.parsing.utils.addresses import parse_address
from ansible.utils.display import Display
import subprocess

display = Display()


class ActionModule(ActionBase):
    ''' Add host to the permanent inventory and run the add_host action_plugin hosts and groups in the memory inventory'''

    # We need to be able to modify the inventory
    BYPASS_HOST_LOOP = True


    def run(self, tmp=None, task_vars=None):
        result = super(ActionModule, self).run(tmp, task_vars)
        module_args = self._task.args.copy()
        self._supports_check_mode = False

        # before running the module, we can add the domain to the vm name
        domain = task_vars.get('domain')
        if domain == '':
            raise AnsibleError("Domain is not set in env")

        # append domain to vm name if it's not already set
        if domain not in module_args['name']:
            hostname =  module_args['name'] + "." + domain
            module_args['name'] =  hostname


        # if template is empty, use the _only_ one availble
        # if there are multiple templates available, fail and list them
        try:
            template = module_args['template']
        except KeyError:
            template = None
        if template == None or template == '':
            if len(task_vars['templates']) == 1:
                module_args['template'] = task_vars['templates'][0]
            elif len(task_vars['templates']) > 1:
                raise AnsibleError("Template was not specified and there are multiple templates available.\nSpecify one of the following: %s" % str(task_vars['templates']))
            else:
                raise AnsibleError("Template was not specified and there are no templates available.\nCreate template by running the vm-template role")



        lab_vm_return = self._execute_module(module_name='lab_vm',
                                             module_args=module_args,
                                             task_vars=task_vars,tmp=tmp)
        #del tmp  # tmp no longer has any effect

        try:
            state = module_args['state']
        except KeyError:
            state = "present"


        # if we have port openings
        # invoke lab_port action plugin with the dicts found in the port_openings: list
        # once we can support dynamic ips, move this below the VM creation
        try:
            port_openings = module_args['port_openings']
        except KeyError:
            port_openings = None
        port_openings_made = []

        if port_openings is not None and type(port_openings) == list and len(port_openings) > 0:

            if module_args['ip'] is None or module_args['ip'] == '':
                raise AnsibleError('port_openings: specified but no static ip defined for VM %s.  This is currently not supported. %s' % (module_args['name'],str(module_args)),tmp=tmp)
            for port_opening in module_args['port_openings']:
                # execute lab_vm module
                #port_opening_return = self._execute_module(module_name='lab_port',task_vars=task_vars,module_args=port_module_args)

                new_task = self._task.copy()
                new_task.args.update(port_opening)
                lab_port_action = self._shared_loader_obj.action_loader.get('lab_port',
                                                                        task=new_task,
                                                                        connection=self._connection,
                                                                        play_context=self._play_context,
                                                                        loader=self._loader,
                                                                        templar=self._templar,
                                                                        shared_loader_obj=self._shared_loader_obj)
                lab_port_return = lab_port_action.run(task_vars=task_vars)
                if lab_port_return['changed'] == True:
                    port_openings_made.append(port_opening)
        
        if state == 'present' and not lab_vm_return.get('failed'):
            # present is the default
            # Add the host to the in-memory inventory by using the meta module refresh_inventory action
            # the add_host is not *really* a module, but rather just an action plugin that
            # runs on the ansible control node, so we can't use the_execute_module to call it.
            # it seems however that adding add_host dict to your result makes it magically pick it up.. took forever to find this out

            # add the host to the shortname group for easier targeting. gotta strip away the domain (everything before the first . )
            group_short_hostname = module_args['name'][:module_args['name'].find('.')]
            lab_vm_return['add_host'] = dict(host_name=module_args['name'], groups=['vms',group_short_hostname], host_vars=dict())
            lab_vm_return['actions'].append("Added %s to ansible in-memory inventory")


            for port_opening_made in port_openings_made:
                lab_vm_return['actions'].append("Port Opening: " + str(port_opening_made))
                lab_vm_return['changed'] = True
        #result.update(lab_vm_return)

        # remove any ssh sockets if the vm is removed
        if state == 'absent' and not lab_vm_return.get('failed'):
            # get the active ssh user from the hostvars
            try:
                user = task_vars.get('user')
                user['name']
            except:
                raise

            #socket_remove = os.system('ssh -O exit -S ~/.ansible/cp/%s-22-%s thehostnamedoesntmatter' % (module_args['name'],user['name']))
            remove_ssh_socket_cmd = ['ssh','-O','exit','-S','~/.ansible/cp/%s-22-%s' % (module_args['name'],user['name']),'foo']
            p = subprocess.Popen(remove_ssh_socket_cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            dhcp_stdout,stderr = p.communicate()
            if p.returncode == 0:
                lab_vm_return['actions'].append("Removed ssh ControlPersist socket ")
                lab_vm_return['changed'] = True

        #display.warning(lab_vm_return['warnings'])
        # This will return whatever the lab-vm module returns, failed or success
        return lab_vm_return
