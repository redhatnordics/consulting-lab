# Make coding more python3-ish, this is required for contributions to Ansible
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from ansible.plugins.action import ActionBase
from datetime import datetime
from ansible.errors import AnsibleError
import ipaddress
import re
from ansible.plugins.action import display

class ActionModule(ActionBase):
    def check_for_role_var(self,rolename, variable):
        '''
        This function checks if a _required_ role variable 
        has been defined.

        '''
        if rolename in self.task_vars['role_names']:
            if not variable in self.task_vars or self.task_vars[variable] is None:
                raise AnsibleError("When running the role %s you have to specify %s: in your lab/env file" % (rolename,variable))


    def run(self, tmp=None, task_vars=None):
    
        self.task_vars = task_vars

        '''
            This action plugin is executed on your own machine before running 
            the setup module that runs at the beginning of every playbook run.

            While most of the modules and roles perform their own checks, we use it to 
            pre-check _early_ to minimize the risk of failing later on in the playbook.
            Since a full setup can take ~30-60min it's reaally annoying to fail due to
            a misconfiguration.

            So whatever common misconfig scenarios we detect, we want to put them into checks here.
            
        '''

        # first of all, check if the dynamic inventory succeded
        # if not, fail early!
        # NOTE: we don't "fail" the inventory if we can't connect. Rather if a config is missing etc.
        # and we don't want the inventory to proceed
        # we pick the weeor up by looking for the labhost var ['errors']
        try:
            if task_vars['inventory_error']:
                raise AnsibleError(task_vars['inventory_error'])
        except KeyError:
            pass

        try:
            if task_vars['inventory_info']:
                for info in task_vars['inventory_info']:
                    display.warning(info)
        except KeyError:
            pass

        # Check if we have a network error from the inventory
        # Only if we are running the create-inventory-network roles should we do something about it
        # we still have the option of running the network role separatly and pass the network directly
        try:
            # Do we have any errors passed from the dyn inventory?
            if task_vars['inventory_network_error']:
                if "create-inventory-networks" in task_vars['role_names']:
                    raise AnsibleError('You are attempting to run a network role, but there was an error reading in the networks: ' + task_vars['inventory_network_error'])
                if "create-inventory-vms" in task_vars['role_names']:
                    raise AnsibleError('You are attempting to run a VM role, but there was an error reading in the networks. Virtual Machines cannot be created if there are no networks: ' + task_vars['inventory_network_error'])
        except KeyError:
            pass
        # no inventory_network_error found, we don't have any passed network issues from the dyn inventory
        # check if we have any networks
        try:
	    # if we have any networks specified, we should verify them
            if task_vars['networks']:
                # check for duplicate subnets and network names
                networks = []
                subnets = []
                for network in task_vars['networks']:
                    if len(network['name']) > 8:
                        raise AnsibleError("Network name %s is too long. maximum 6 char" % network['name'])

                    if not "subnet" in network:
                        raise AnsibleError("Network %s doesn't have a subnet specified")

                    try:
                        ipaddress.ip_network(network['subnet'])
                        if network['subnet'] in subnets:
                            raise AnsibleError("(%s) subnet %s already in use" % (network['name'],network['subnet']))
                        subnets.append(network['subnet'])
                    except ValueError:
                        raise AnsibleError("Network '%s' does not have a valid subnet (%s)" % (network['name'],network['subnet']))

                    # type defaults to restricted if not specified
                    if "type" in network:
                        if not network['type'] in ('restricted','unrestricted','nat'):
                            raise AnsibleError("Network '%s' have an invalid type (%s).\nNeed to be either 'restricted','unrestricted','nat'" % (network['name'],network['type']))
                    else:
                        network['type'] = "restricted"

                    if network['name'] not in networks:
                        networks.append(network['name'])
                    else:
                        raise AnsibleError("Network name '%s' is used multiple times" % network['name'])

	# we didn't have any items in the network list
	# that's ok as long as we don't run the create-inventory-networks or create-inventory-vms roles
        except KeyError:
            if "create-inventory-networks" in task_vars['role_names']:
                raise AnsibleError('You are attempting to run a network role, but there was an error reading in the networks: ' + task_vars['inventory_network_error'])
            if "create-inventory-vms" in task_vars['role_names']:
                raise AnsibleError('You are attempting to run a VM role, but there was an error reading in the networks. Virtual Machines cannot be created if there are no networks: ' + task_vars['inventory_network_error'])

        if "subman" in task_vars['role_names']:
            if "rh_username" in task_vars or "rh_activationkey" in task_vars:
                if not "rh_username" in task_vars or task_vars['rh_username'] is None:
                    raise AnsibleError("'rh_username:' in lab/env needs to be specified and contain a value")
                if not "rh_password" in task_vars or task_vars['rh_password'] is None or task_vars['rh_password'] is "null":
                    raise AnsibleError("'rh_password:' in lab/env needs to be specified and contain a value")
            else:
                raise AnsibleError('You will need either a rh_username + rh_password or rh_activationkey to subscribe')


        # Most of the vars that defines the lab are passed in as hostvars to labhost
        # It's unnecessary to run these prechecks on any other host than labhost
        # So run this checks only if we are running the setup for labhost
        if task_vars['inventory_hostname'] == 'labhost':
            # environment
            # RHSM

            if not "domain" in task_vars or task_vars['domain'] is None:
                raise AnsibleError("'domain:' in lab/env needs to be specified and contain a value")
    
    
            # VMS check
            # and task_vars['vms'] in this case checks if the list is empty
            if "vms" in task_vars and task_vars['vms'] is not None and task_vars['vms']:
                # we can only create VM's if we have networks
                try:
                    network
                except NameError:
                    raise AnsibleError('You are trying to create Virtual Machines, but no networks have been defined in your networks file')
                # list to check that we are not using the same ip twice
                ips = []
                for vm in task_vars['vms']:
                    # check mandatory network
                    try:
                        # check if the network the vm wants is actually defined in networks
                        vm_net = vm['network']
                        if vm_net not in networks:
                            raise AnsibleError("VM %s defined network '%s' is not defined in networks" % (vm['name'],vm['network']))

                    except KeyError:
                        raise AnsibleError("VM %s is missing 'network: ' " % vm['name'])
                    # ip check
                    try:
                        ip = vm['ip']
                        if not re.match(r'[0-9]+(?:\.[0-9]+){3}',ip):
                            raise AnsibleError("VM %s ip: %s is not a valid ip. " % (vm['name'],ip))
                        #if ipaddress.ip_address(ip) not in ipaddress.ip_network(vm_net)):
                        #    raise AnsibleError("VM %s ip: %s is not withing the subnet %s" % (vm['name'],ip,vm_net)

                        # check that the ip isn't already in use
                        if ip in ips:
                            raise AnsibleError("VM %s ip: %s is already defined for a different VM" % (vm['name'],ip))
                        else:
                            ips.append(ip)
                    except KeyError:
                        # dynamic ip
                        pass

        # Other role var checks         
        self.check_for_role_var("openvpn","openvpn_gateway")
        self.check_for_role_var("lab-configure-client","openvpn_gateway")


        # Running the vanilla setup
        super(ActionModule, self).run(tmp, task_vars)
        module_args = self._task.args.copy()
        module_return = self._execute_module(module_name='setup',
                                             module_args=module_args,
                                             task_vars=task_vars, tmp=tmp)
        return module_return
