# OpenShift 3-2-2

3 Masters  
2 Infra nodes  
2 Worker nodes  
-
1 jumphost/bastion host  
1 Proxy  
2 LoadBalancers  

You need quite a lot to get a cluster up. 

This example will create the infra for you. The installation and configuration of OpenShift is up to you :) 
