## Define the networks

Now we will define the networks that should be created in virtual environment.  
With the [previous step](../setting_up_the_environment) completed we should now be able to create networks in our labenvironment. 

We define networks in the `lab/networks` configuration file.  
A network entry requires at least a `name:` and `subnet:`  

`subnet:` needs to be in the prefix format like `subnet: 123.123.123.123/24`.  

We use the concept of different network _types_ that behave differently.  
In short:  
`restricted` Requires you to open up the ports explicitly between hosts and networks for a true lab environment.  
`unrestricted`  Forwards/routes everything to the hosts on the networks, no limitation.  
`nat` act as unrestricted network but also have DNAT configured and access to internet. Much like the usual libvirt experience.  
`type:` Will default to `restricted` if not supplied.   

Read more about the different network types in [Network](Network) section.  

You enter a network item like this: 

```
# lab/networks

- name: <network-name>
  type: <restricted|unrestricted|nat>
  subnet: 10.0.0.0/24

``` 
Every list item here will be created when we run the *role* [`create-inventory-networks`]() which can be run adhoc with the [`create-networks`]() playbook or included as part of a larger lab setup playbook. 

----

**As an example, define the following networks in your `lab/networks` file.**

```
# lab/networks

# restricted network
- name: mgmt
  subnet: 10.50.0.0/24
  type: restricted

# internet access
- name: dmz
  subnet: 172.16.122.0/24
  type: nat
``` 

This will in the end give us two libvirt networks where we can put our VM's on.  
The `"mgmt"` network that is restricted and the `"dmz"` network that has internet access.  

Hosts on the `"dmz"` network will not be able to reach hosts on the `"mgmt"` network, but hosts on the `"mgmt"` network can reach hosts on the `"dmz"` network.  
However, hosts on the `"mgmt"` network can't reach the internet without the use of a proxy.

!!! note
    Again, the type restricted means that the network is locked down and you have to open up for ports that you need (like in a real secure environment).
    You can assign these port openings on the network entry with `port_openings:`   
    This will be covered in the later stages.  

## Create the networks

*Now, create the networks by running the `create-networks.yaml` playbook* 
```
$ ansible-playbook -i lab/ create-networks.yaml
```

If you hit any issues, please see the [Common Issues](../common_issues.md).  

