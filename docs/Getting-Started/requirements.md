# Requirements

Before we get on with [Setting up the environment](Getting-Started/setting-up-the-environment)

The roles and overall solution has been developed and tested with Ansible 2.8.
Make sure that you use Ansible 2.8 or later. 
```
$ ansible --version
```

If you're not running the latest version follow the offical [Ansible docs](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) on how to install or upgrade to latest Ansible.

WIP: 
The roles have some dependencies that you don't get out of the box with ansible.  
Make sure to install them:
```
$ pip install -r requirements.txt
```

### MacOS
!!! warning
    Consulting Lab is primarily developed on Fedora and RHEL workstations.  
    While we aim to support Mac users too, milage may vary.

For MacOS users, you _might_ need some additional stuff to get the libvirt module:
```
brew install pkg-config libvirt coreutils coreutils
pip3 install PyYAML Jinja2
```

Add your SSH key to the ssh-agent:


## The Labhost


The only requirement for the labhost is that it's a clean installation of either Red Hat Enterprise Linux 7 or CentOS 7 and that it has direct access to Internet.
(And preferably a bare-metal server big enough to hold your workload, as nested virtulization is meh)

!!! note
    If you are planning to use a Hetzner server, we have a quick guide on that [here](/docs/hetzner-setup.md).  
    We have roles that can install a Hetzner server booted into rescue/setup mode from scratch.  

## Red Hat user credentials

We are required to subscribe both the labhost itself and all the VM's that we will create.  

Make sure that you have a Red Hat account and available subscriptions to entitle **Red Hat Enterprise Linux Server** for your labhost and the VM's.  
It can be any subscription, as long as it provides RHEL entitlement. (Developer SKU included)

```
# subscription-manager list --available 
...
Subscription Name:   XXX
Provides:            XXX
                    ...
                     Red Hat Enterprise Linux Server   <--
                     ...
Provides Management: Yes
Available:           9937
Suggested:           1
Service Level:       Self-Support
Service Type:        L1-L3
Subscription Type:   Standard
Starts:              05/26/2011
Ends:                01/01/2022
System Type:         Physical
```



 

