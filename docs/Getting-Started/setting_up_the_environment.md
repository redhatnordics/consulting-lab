

## Checkout the repository

We will now clone the repo and prepare your lab environment by modifying configuration files. 
Begin by checking out the git repository locally:

```
git clone  git@gitlab.com:redhatnordics/consulting-lab.git
cd consulting-lab
```


## Setting up the environment

The idea here is to model up your lab environment within the configuration files. 
When the environment grows or changes, we edit the configuration and we run Ansible to make it so. 

Everything is kept within the `lab/` directory.  
Within this directory you should have at least 4 files:  

| | |
--|--
`dyn.py`  | The [Dynamic Inventory](missing-link) that will act as the engine for this lab setup.
`env`     | YAML configuration file. Here you define the external address of the labhost, the user that will be used to connect to the labhost and VM's, RH credentials etc.
`networks`| YAML file where we define the networks we want in the lab.  
`vms`     | YAML file where we define the VM's we want in the lab. 

The dynamic inventory `lab/dyn.py` will read in the adjacent files during execution.  
You can read more about the inventory setup in the [Inventory](The-Inventory) section, but for now you can follow along this page.  


### Edit `lab/env` 

#### `labhost_wan_address`
First step should be to put your labhost in the `labhost_wan_address:` in `lab/env`. If your labhost is resolvable you can use the FQDN of the box, but an IP address works just aswell.  
This will used in the generated `.ssh/` config and for the labhost host object in the ansible inventory.  

```
# lab/env

labhost_wan_address: "134.4.56.1"
```
alt:
```
# lab/env

labhost_wan_address: "bigbox.myowndomain.org"
```
------

#### `domain`

Next we add the `domain:`  
We require a subdomain to be set in order for the SSH configuration to work.  
This will also be appended to any VM that you create using the repository roles and modules.  

!!! note
    This does not ***need*** to be a domain that you own, but it's important that the domain is not owned by someone else and resolvable, as this can cause multiple issues during lookups

```
# lab/env

domain: consulting.lab
```

------

#### Lab user

If you don't want your *current* user on your workstation, along with it's `.ssh/id_rsa` private key to be used, you have to set the `user:` section.
Here you have the option of specifying a username and a public key-pair that should be used when you connect to your labhost _and_ VM's.  
This will be added to you SSH configuration with an extra config file in `.ssh/consulting-lab.conf`. 

!!! warning
    As of now you can't opt out of the appending of this file.
    Alot of the automation depends on the fact that we can easily connect to both the labhost and the VM's and the additional ssh config ensures that.  
    However, we will add a way for you to opt out of this and manage your ssh config yourself soonish!

If you haven't prepared your labhost with these user credentials already, you can quickly do so with the `lab-user` role.
It will pick up the defined user and add it to the labhost with sudo NOPASSWD privleges.
You may have to swap the `user: root` to whatever is appropriate in your case and use the ansible-playbook options to authenticate and escalate.  
The point here is that you shouldn't need to do that anymore afterwards.. 

eg. 
``` bash
ansible-playbook -i lab/ adhoc/lab-user.yaml --ask-pass
```

----

#### Red Hat credentials

We will register the labhost and the VM's to Red Hat.  
This is done by the [`subman`](https://gitlab.com/redhatnordics/consulting-lab/tree/master/roles/subman) role and is used as a dependency in many places.  

Since we want to be able to keep our passwords a secret, we utilize Ansible Vault.  
And to do that you will first have to enter your Red Hat username and password in 
the `all:` group variables.  

Edit your `lab/group_vars/all/rhsm` file and fill in your credentials     
```
# lab/group_vars/all/rhsm

rh_username: "consultant@redhat.com"
rh_password: "supersecret"
```

You can then choose to encrypt this file with `ansible-vault`. 

```
$ ansible-vault encrypt lab/group_vars/all/rhsm

```

!!! note 
    This does mean that you have to supply your Ansible Vault password for each playbook run.  
    Read more about how this works in the [Ansible documentation](https://docs.ansible.com/ansible/latest/user_guide/vault.html) and how you can supply it automatically with a passwords file. 

## Include the Consulting Lab SSH config

By using the dynamic inventory `lab/dyn.py` it reads the variables supplied in `lab/env` and creates 
a SSH configuration for you to make things smoother. 

!!! note 
    The SSH config includes a wildcard catch for your domain.  
    This means that every time we try to connect to a VM using it's FQDN, a configuration is applied that makes SSH try to first connect directly (implying that you are connected to VPN) and failover to using your labhost as a Jumphost.

    It will also make sure that when we do `ssh labhost`, labhost actually resolves to either your VPN gateway or your external address `labhost_wan_address:`.   
    First attempt is to connect to your VPN connection and then fail over to the external address.  
    This allows for a flexible setup where you can either be on VPN or connect directly. 

    Also some connection tuning that has been proved useful under shaky connections as well as StrictHostKeyChecking for when you tear VM's up and down frequently.


The config is found `~/.ssh/consulting-lab.conf` and needs to be included into your regular `~/.ssh/config` file. 

Edit the file `~/.ssh/config` and append the line **at the very top**. 
``` bash
# ~/.ssh/config

Include consulting-lab.conf


--- the rest of your ssh config ---
```

## Run the setup

With the `lab/env` configured we are now ready to run the initial setup of the labhost.  
This is simply running a set of roles that will:  
- Setup of the labhost server itself with some basics.  
- Install and configure libvirt. Add your user to the libvirt group for virsh connection.  
- Configure iptables [`role/libvirt`]()  
- Configure OpenVPN (optional)  

!!! note
    These playbook requires that you've setup your user on the labhost beforehand.
    `become: true` means that your user will escalate to root privleges using sudo. 
    See above for instructions on how to quickly distribute this to the labhost. 

The following playbook would install the basics of preparing your labhost for networks and VM's: 
You find it in the root of the repo [`lab-basics.yaml`](). This will run the `lab-basics` wrapper role on the labhost. 
    
``` yaml
# /lab-basics.yaml
- hosts: labhost
  become: true
  roles: 
  - lab-basics
```

Run it:
``` bash
ansible-playbook -i lab/ lab-basics.yaml
```

!!! note
    Should you hit any errors running the playbook, see the [Common Issues](../common_issues.md) page to see if you can find a reason for it.  
    Otherwise raise an Issue here [https://gitlab.com/redhatnordics/consulting-lab/issues](https://gitlab.com/redhatnordics/consulting-lab/issues) if you feel it's a bug.  
    Or hit us up at #nordic-consulting-lab.
