virt-manager

As soon as you've installed and configured libvirt on the labhost
by running either the `lab-basic` role or the `libvirt` component role, you
should be able to setup your local virt-manager to the labhost. 

virt-manager is a , and is shipped as part of RHEL Desktop and Fedora.

!!! note 
    If you are a Mac user, you might wanna check out [https://github.com/jeffreywildman/homebrew-virt-manager](https://github.com/jeffreywildman/homebrew-virt-manager)

If you've included the [SSH config](../setting_up_the_environment/#include-the-consulting-lab-ssh-config) it should just be a matter of creating
a new connection and use the SSH method and the hostname labhost.  
Your specified user should be prepped with the proper permissions for libvirt. 

<img>

The VM's are configured to use SPICE for the graphical environment, and it
all works over SSH.


