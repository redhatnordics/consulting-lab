## Create a VM template

Before we can create any Virtual Machines we need a "template" image.  
When we create vm by using the `lab_vm:` module, we copy the image template and then sysprep it.  
This allows for very rapid deployments.  

For this step we will simply run the `vm-template` role as it is that will create a default RHEL7 image for us.
You can read up more on how you can customize and create your own template in the [Templates](../../templates) section.

Run the `vm-template` role by running the playbook `adhoc/create-vm-template.yaml`.
``` bash
ansible-playbook adhoc/create-vm-template.yaml -i lab/
```

This will pull down the latest Boot ISO from Red Hat CDN, create a Virtual Machine and install it against the latest rhel-7-server-rpms repository from Red Hat CDN. 
The resulting image will be sysprepped and your user will be added so that we can connect right away when your clone is up. 

## Define VM's

With networks defined and an VM template ready we can define what Virtual Machines should be created on the networks. 

VM's are created with the module [`lab_vm`](https://gitlab.com/redhatnordics/consulting-lab/blob/master/library/lab_vm.py), but we use the role `create-inventory-vms` to create the VM's you define the `lab/vms`.  

The role can be called with the playbook `create-vms.yaml` found in the root directory, or be included as part of a larger lab setup playbook. See the examples.      
!!! note
    When running the `create-inventory-vms` role (or the `lab_vm` module for that matter), it will detect what VM's are already present and will not take any action on those.  

It is still possible however to create a VM on the fly with either `adhoc/create-vm.yaml` or invoking the `lab_vm` module.
You will still be able to connect to it as the dynamic inventory will pick it up, but in a way we drift a way from the _defined_ lab that way. Each to his own! But for now we use the `create-inventory-role` and the `create-vms.yaml` playbook.

----

To define a VM, edit your `lab/vms` file as yaml list items. 

A VM entry _requires_ the following vars:

`name:` the name of the VM.  
**The subdomain specified in `lab/env` will automatically be appended to the hostname of the vm**. 
`network:` the name of the network created in previous step. ie. the network must be defined in your `lab/networks` file!

A minimal VM definitions file for two VM's could look like this:
One host  
``` yaml
# lab/vms

- name: hostA
  network: mgmt

- name: hostB
  network: nat

```

You have additional options at your disposal for how your VM should be created.
See [Virtual Machines](../virtual-machines.md) page for more extensive info on how you can define your VM's and how they are created.  

For now, we can copy the following: 
``` yaml
# lab/vms

- name: hostA
  cpu: 2
  memory: 2048
  network: mgmt

- name: hostB
  memory: 4096
  network: mgmt
  ip: 10.50.0.10
```
----

This will in the end be two VM's living on the same network.    
HostB has a set `ip:` of 10.50.0.10, while HostA does not.   
For HostA the ip will by dynamically assigned.   


## Create the VM's 

Allright, once you've defined your `lab/vms` file with all the vm's that you anticipate needing for your lab, you run the playbook `create-inventory-vms.yaml`.  

``` bash
ansible-playbook create-vms.yaml -i lab/
```
Once the playbook has finished, you should now be able to ssh to your new VM's with their FQDN. ie. the VM `name:` + `domain:`.
Give it a go:   
``` bash
$ ssh hostA.consulting.lab
```

!!! note
    If you hit any issues, please see [Common Issues](../common_issues.md) or file an Issue here: [https://gitlab.com/redhatnordics/consulting-lab/issues](https://gitlab.com/redhatnordics/consulting-lab/issues)
