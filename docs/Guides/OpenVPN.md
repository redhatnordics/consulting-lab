
Set up a OpenVPN server on your labhost that has access to all your lab networks and vm's.  

We provide the [`openvpn`](https://gitlab.com/redhatnordics/consulting-lab/tree/patch-june/roles/openvpn) role.  
It will install and configure OpenVPN on your labhost and make sure it can reach all the networks that you create within the lab.  
By default, it will also attempt to set up a Network Manager profile on your workstation. If it can't, it will settle of placing a OpenVPN configuration file in your home directory (~/openvpnconfigs/labhost). 
!!! note
    You can't opt out of the automatic NetworkManager setup yet.

## Single user 

As of now, the roles configures the static-key https://openvpn.net/community-resources/static-key-mini-howto/. 


## Setup

Either include the role to your larger playbook
```
- hosts: labhost
  become: true
  roles:
  - openvpn 
```

Or you can simply run the `adhoc/openvpn.yaml` playbook:

```
$ ansible -i lab/ adhoc/openvpn.yaml
```

