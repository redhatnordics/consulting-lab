
When you are running [`restricted`]() networks to simulate a common customer environment, the need for a http proxy arrives as you will still need access to Internet resources like the Red Hat CDN. 

Our default choise is to use a [squid](www.squid-cache.org) proxy. 

The proxy host itself needs access to Internet, and we can give it that by placing it on a [`nat`]() network where it has unrestricted access to Internet.  
Hosts on `restricted` networks can then configure their applications to use the proxy. 

```

                        |nat network|          |restricted networks|
    Internet -------   proxy.lab.org:3189  -----   vm1.lab.org

```

To set this up we can use the following playbook and pattern. 

## Define the proxy network

First, we need to set up a NAT network for the proxy to be on.  

Open your `lab/networks` file and add a `nat` network. 
You can name this to `proxy` if you'd like. 

```
# lab/networks
- name: proxy
  subnet: 192.168.60.0/30
  type: nat

```

You can run the playbook `create-networks.yaml` now to create the network.  
Or you can wait and run it all together later. 

```
$ ansible-playbook -i lab/ create-networks.yaml
```

Notice the small subnet `/30`. This only allows for the host addresses `192.168.60.1` (which is taken by the labhost gateway) and `192.168.60.2`.

## Define the proxy VM

Next we need to define the VM that the proxy will run on.
Append the following to your `lab/vms` file: 

```
# lab/vms

- name: proxy
  network: proxy
  ip: 192.168.60.2

```

You can go ahead and run the `create-vms.yaml` playbook now if you want: 
```
$ ansible-playbook -i lab/ create-vms.yaml
```


## Configure VM

With the VM up we will now turn it into a Squid proxy.  

Have a look at the playbook `create-proxy.yaml`.

```
---
# This playbook will configure your proxy host
# to run a squid proxy.
# All existing VM's will be configured to use the proxy
# for their subscription-management (rhsm)

- hosts: proxy
  become: true
  roles:
  - subman
  - squid

- hosts: vms,!proxy
  become: true
  tasks:
  - import_role:
      name: subman
    vars:
      subman_proxy_hostname: "proxy.{{ domain }}"
      subman_proxy_port: 3128
```

The first part of the playbook targets the *new* proxy vm with `hosts: proxy`. 
It first runs the [`subman`]() role in order to subscribe the VM to Red Hat so we get access to RHEL repositories.  
Next it runs the [`squid`]() role which will install and configure the actual squid proxy. 

Afterwards we should have a working proxy on `proxy.yourlabdomain.org` listening on port 3128.  

!!! note 
    Our dynamic inventory will pick up the shortname of the new VM so you can use the shortname `proxy` that we specified in the `lab/vms` file and ansible will know about it.  

The second play in the playbook targets all of our VM's **except** the proxy.  
Here we can run roles that configures the client proxy configuration.  
In the playbook you see that we run the `subman` role again, but this time we pass proxy configuration that targets `proxy.{{ domain }}`. 
This way all of our VM's will use the new proxy for subscription-manager. 

Run the playbook `create-proxy.yaml`:
```
$ ansible-playbook -i lab/ create-proxy.yaml
```




