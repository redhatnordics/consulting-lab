# Connecting to your Lab and VM's. 

After setiing up the Labhost and creating all the VM's, we want to connect to them over SSH.  
The tricky part here is that early on, your machine have *no idea* about the VM's we just created.  
These are VM's that run on a hypervisor on virtual networks that only exist within the labhost domain. 
So we need to solve two problems really; **how will your workstation know about the VM's** you've just created and **how can I connect to them?**. 

The consulting-lab provides two *ways*, a SSH jumphost solution and the VPN solution.  
We will also see that both of these metods relies on the fact that the labhost knows how to *find* the VM's. (maybe not). 

## labhost /etc/hosts

When we create VM's with the [`lab_vm:` module ](/virtual-machines/#module-lab_vm) it automatically adds the hostname to the labhost `/etc/hosts`.  
So if you were to create the following VMs:
```
# lab/vms
- name: vm1
  network: mgmt

- name: vm2
  network: mgmt
  ip_address: 10.0.0.5
```
with your `domain:` in `lab/env` set to `example.org`
``` 
# lab/env
domain: example.org
```

Labhost `/etc/hosts` can look like this.
```
# /etc/hosts
123.123.123.123 labhost

10.0.0.14 vm1.example.org 
10.0.0.5  vm2.example.org 
```
!!! note
    Note that we didn't supply an `ip_address:` for vm1 but we still get a "static" ip set.  
    This IP address is fetched from the libvirt dhcp when we created the VM with `lab_vm:`.  
    The record is dynamically assigned but statically set so we ensure the vm keeps that ip.
 
This way the labhost knows about the VM's that lives on the hosts. 
But how does your workstation know about them? 

## SSH proxy and .ssh config (VPN-less method)

This method is simply using your labhost as a ssh jumphost.  
Whenever we want to connect to a VM over ssh, we do it via the labhost.  
We do that by employing a wildcard scheme in your SSH configuration.  

Basically, whenever we connect to `*.yourlabdomain.org` ssh should send the traffic via labhost. 
Since we are proxying, we do not *need* to be able to resolve the address, we leave that to the jumphost. 
The jumphost in our case is the labhost, which keeps a record of all the VM's in it's `/etc/hosts` file, so it can always resolve the VM's.  

To build on the example from before, we proxy everything matching \*.example.org to the labhost, and the labhost will look up the ip of the VMs in /etc/host, the jump will happen.

```
                                                 +---------------------------------------------------+
     Connecting to VM's                          |                                                   |
     using SSH tunnel                            |                        Virtual Machines           |
                                                 |                                                   |
                                                 |  Labhost                                          |
                                                 |  =======             +-------------------+        |
                                                 |                      |                   |        |
                                                 |                      |                   |        |
                                                 |                      |  vm1.example.org  |        |
                                                 |                      |                   |        |
                                                 |   /etc/hosts         |                   |        |
   +----------------+                          10.0.0.5 vm2.example.org |                   |        |
   |                |                            |                      +-------------------+        |
   |                | $ ssh vm2.example.org      |  (2)                                              |
   |  Workstation   | +----------SSH Tunnel-------+                                                  |
   |                | +---------------------------------------+                                      |
   |                |                            |            |                                      |
   +----------------+    (1)                     |            |         +-------------------+        |
  X                  X                           |            |         |                   |        |
 X                    X                          |            |         |                   |        |
+----------------------+                         |            |         |                   |        |
                                                 |            +-------->+  vm2.example.org  |        |
   ~/.ssh/config                                 |                   (3)|                   |        |
   Host *.example.org                            |                      |                   |        |
     ProxyCommand ssh -W %h:%p labhost           |                      +-------------------+        |
                                                 |                                                   |
                                                 +---------------------------------------------------+

 (1) Connecting to vm2.example.org will first setup the ssh connection to the labhost

 (2) Once the the SSH tunnel is up, the `ssh vm2.example.org` command will run on the labhost.
     The labhost which executes the command knows about the VM from /etc/hosts, so the lookup will succeed.

 (3) The credentials and settings are forwarded over the tunnel, and your workstation can sucessfully connect  
     since the VM's are prepared with the public key on the users you've configured.
```

### SSH config

So what configures your SSH config to work this way?  
The obvious answer here is ofcourse, your dynamic inventory! :)  

Everytime we run the python based dynamic inventory, it will ensure that the file `~/.ssh/consulting-lab.conf` is present
It's up to you to make sure that it's included in your `~/.ssh/config` includes it at the top of the file.  


The SSH config includes a wildcard catch for your domain.  
This means that every time we try to connect to a VM using it's FQDN, a configuration is applied that makes SSH try to first connect directly (implying that you are connected to VPN) and failover to using your labhost as a Jumphost.

It will also make sure that when we do `ssh labhost`, labhost actually resolves to either your VPN gateway or your external address `labhost_wan_address:`.   
First attempt is to connect to your VPN connection and then fail over to the external address.  
This allows for a flexible setup where you can either be on VPN or connect directly. 

Also some connection tuning that has been proved useful under shaky connections as well as StrictHostKeyChecking for when you tear VM's up and down frequently.

!!! note 
    Previously we did employ an `ansible_ssh_connection:` scheme for ansible to connect to your inventory vms.  
    While this worked, it did nothing for your non-ansible ssh connections.  
    This way configure your users ssh config to always be able to connect to your VM's. 


The config is found `~/.ssh/consulting-lab.conf` and needs to be included into your regular `~/.ssh/config` file. 

Edit the file `~/.ssh/config` and append the line **at the very top**. 
``` bash
# ~/.ssh/config

Include consulting-lab.conf


--- the rest of your ssh config ---
```

!!! note 
    We will include a seperate page for the SSH config topic ASAP

# VPN and dnsmasq on labhost

When connected using the VPN connection there is no need for a jump, and you can access to your VM's directly.   
You can read more about how to setup the VPN and your workstation in the [OpenVPN guide](/Guides/OpenVPN)

However, your workstation still need to _know about_ (ie. be able to resolve) the VM's in order to connect to them.  
Since we are not using the jumphost as a jump we have to get the IP's in some other way.  
Luckily there is just the solution for that.. 

The openvpn role will install a dnsmasq service on your labhost and listens on the VPN interface.  
dnsmasq reads `/etc/hosts` of the labhost, and every VM that we add ends up in there.  
So when connected with VPN we want to use the labhost as DNS aswell.  
With this in place, we can both lookup and connect to the VM's. 

The DNS address is the one of the VPN gateway interface.  
This is by default 10.99.99.1.  

If you were able to run the linux NetworkManager tasks during the [OpenVPN setup](/Guides/OpenVPN) this should be configured automatically, so that every time you connect with to the Lab VPN the OpenVPN gateway is used as primary DNS server.  

```
                                                 +---------------------------------------------------+
     Connecting to VM's                          |                                                   |
     using VPN tunnel                            |                        Virtual Machines           |
                                                 |                                                   |
                                                 |  Labhost                                          |
                                                 |  =======                                          |
                                                 |                                                   |
                                                 |                                                   |
                                                 |        --------------                             |
                                                 |        | /etc/hosts |                             |
                                                 |        |            |                             |
   +----------------+                            |        |  dnsmasq   |                             |
   |                |                            |        --------------                             |
   |                |         (1)                |           (2) dns                                 |
   |  Workstation  tun0|---------VPN Tunnel----|tun0          |                                      |
   |                |                            |            |                                      |
   |                |+----------------------------------------+                                      |
   +----------------+                            |            |         +-------------------+        |
  X                  X                           |            |         |                   |        |
 X                    X                          |            |         |                   |        |
+----------------------+                         |            |   (3)   |                   |        |
                                                 |            +-------->+  vm2.example.org  |        |
   ~/.ssh/config                                 |                      |                   |        |
   Host *.example.org                            |                      |                   |        |
     ProxyCommand ssh -W %h:%p labhost           |                      +-------------------+        |
                                                 |                                                   |
                                                 +---------------------------------------------------+

 (1) The VPN connection is established and the workstation now have direct access
     to the lab networks.

 (2) The VPN profile sets up the labhost as it's primary nameserver.  
```









