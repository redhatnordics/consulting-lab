# Hetzner 

Thus guide will show steps to take to order a server and prepare it for lanhost use.  

Order a server on hetzner that has large enough specs for your lab purpose.  

After a while you should recieve a mail from Hetzner saying your host is ready.  
The host is now in Hetzners "rescue" mode. From here you can reinstall the host using the custom installation tool.  
In this repo you have the [hetzner-provision](/roles/hetzner-provision) role that will automatically do this for you.  
This is highly optional and you might wanna do this yourself, but do check out the role and the [examples](/examples/) how you can automate this.  
Very useful for starting over completely if you borked something or just wanna start fresh.  
  
The host should be prepped with the public key you gave during the setup wizard, so you can login directly as root.


## Reset Hetzner labhost for reprovision.






