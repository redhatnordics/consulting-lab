Consulting-Lab 
==============

As Red Hat consultants we often need to test different setups and scenarios  
with Red Hat products. The scenarios themselves often involve multiple networks  
that are heavily restricted and we need to take into account things like proper  
central authentication, DNS, proxies etc. Setting up the lab can be very time  
consuming in itself and eat away from the time you've reserved to prepare or  
you just need to quickly test something while at the customer and there is no  
viable test environment.    

## So what is this?

Consulting Lab is an idea and a set of tools for you to setup a Lab environment  
aimed at the Red Hat consultant.  The idea is centered around having a large  
box with enough resources to run your whole virtualized environment.  By using  
ready available tools and software like libvirt, openvpn, iptables etc.  
together with Ansible we build a complete lab environment.  

You model up your lab environment with YAML and run playbooks to spin it up  
or down.  The automation will handle things as:  
- Network creation with different types of restrictions  
- Port openings  
- VM creation and templating  
- IP assignment of VM
- Direct or VPN connection to your lab VM's.

Many of us already created something similar in some shape or form and  
Consulting Lab started as an initiative to gather our different Ansible roles  
into something a bit more generic. 
Consider giving this a go ([Getting Started](getting_started.md)) and  
check out some of the [Examples](examples.md), see if it fits your needs for your projects.  
If not, we'de love your help shaping it to be better. (See [Contribute](contribute.md))

# So why not cloud?  
While Cloud is great, there are times where cloud is not the perfect fit.
Many of the customer scenarios we need to lab with will not run in the cloud in the end
but rather in the customer datacenter. Sure, you can turn your cloud environment
into a locked down old-schoold dc environment, but that kinda defeats the purpose donnit?

Also, there is the factor of cost. Cloud can quickly become expensive.  
Maybe not for your production workload, but for a personal lab environment that you
may want to keep the environment up and available for a longer time? 

The purpose of this project is not to be any replacement for Cloud, it's 
rather to fill a very specific need for a Consultant (or any other role really) 
that quickly needs to spin up a lab for whatever purpose, on any commodity hardware
that you might own or have access to. 

So thus far we've based the project around the idea that you have access to a
large hardware server that you want to turn into a lab.  
Many of us are renting servers from places like OVH and Hetzner and we do  
have some automation specific to those providers.  
However it's not specific to those type of services. All that should  
be required is large enough hardware to run what you want to run,
an internet connection and a clean EL7 installation.  
See [requirements](requirements.md)

# So how does it work?

If you go have a look at the [repo]() you will see that it's basically an Ansible repo.
You'll find playbooks, roles and an inventory.  
The idea is that you work out of your own clone of this repository and model
up your environment using certain inventory configuration files and then
you run the lab roles using either the provided playbooks or playbooks that you
assemble yourself. 
Once you want to change something in your environment, add a new vm, new network,
you add it and run Ansible again. 
If you want to tear it all down and up agian, you can run a playbook that does just that.

The repo differs somewhat from a vanilla Ansible setup, in that we use
some dedicated config files that are read in by a custom dynamic inventory.
If you follow the [Getting Started](Getting-Started) you'll quickly see how this works. 

The roles themselves are geared towards setting up and configuring your server
to turn it into a "Labhost", aswell as creating the virtual environment for your lab.
Network with that can be more or less restricted, and Virtual Machines that are
quickly spun up using template. according to your specs, on the networks that you've defined.

You'll also find roles and playbooks to spin up useful resources like OpenVPN for 
connecting to your lab if you want.In the works are 

!!! note 
    We try hard to make the packaging nice and useful so it doesn't just 
    end up being a repo on GitLab with a bunch of roles in it. 
    If you feel there is a lack of usability or any other issue, let us know on our [GitLab Issue tracker](), or even better, consider [Contributing]()

Checkout this ascii cast to see a setup to get a feel for how it looks.
Or get on it right away with the [Getting Started](getting-started).

# What is it not?

The aim is to provide you with the infrastructure for your lab environment fast.  
What to lab with is up to you. So you might not find a complete OpenShift lab in here, rather you will find 
examples on how to provide the base infra for an OpenShift lab; the VM's, the networks, loadbalancer etc. in order for you to spin up OpenShift *on your own*. 
After all, thats the lab right? 
