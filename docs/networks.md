!!! warning WIP
When we talk about networks, we talk about Libvirt virtual networks, and when we create them it's libvirt doing the heavy lifting.  
It will create the virtual bridges and connect your VM's to them.  

The usual libvirt network would either be the "NAT" or the "Bridged" types ([https://wiki.libvirt.org/page/Networking#NAT_forwarding_.28aka_.22virtual_networks.22.29)](https://wiki.libvirt.org/page/Networking#NAT_forwarding_.28aka_.22virtual_networks.22.29)
What libvirt does is to apply certain iptables rules on your hypervisor for the network.  
For instance, with the NAT network, it makes sure that the proper PREROUTING rules are applied in the nat table so masquarade happens and the VM's can reach the Internet, aswell that there are FORWARD rules in place so that VM's on it can communicate with other VM's on different networks.  

However, when we create them we are using the "Open" network type, which basically means that libvirt will not any apply any iptables rules at all.  
So from the beginning, these networks are completely isolated.  
The VM's on the network has the labhost as it's gateway, but there are no FORWARD rules in place which means that the gateway (labhost) won't route the traffic anywhere.  
From there on we build our own network types, namely `restricted:`,`unrestricted:` and `nat:`.  
We do this to better emulate how a real environment would look like at a customer and `restrictied:` is the default network type choice.  



`restricted:` is implemented with 
