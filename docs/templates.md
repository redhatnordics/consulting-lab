When using the `lab_vm:` module to create a VM whats actually happening is that  
the module calls on the utility `qemu-img` with:

``` bash
qemu-img create -f qcow2 -b /var/lib/libvirt/images/templates/' + template + '.qcow2 /var/lib/libvirt/images/' + vm_name + '.qcow2'
```
[]()

Basically, we are cloning an already existing image, a template.  
`lab_vm:` will do further preperation with the VM that isn't covered here.  

The template to be used is specified with the `template:` option:
``` yaml
# lab/vms

- name: custom-template-vm
  template: template-custom
  network: test
  cpu: 2
```

The templates you can specify here is determined by the list of templates reported  
by the inventory. When you run a playbook using the dynamic inventory (`lab/dyn.py`)  
it will fetch every domain (in libvirt speak) that begins with the name `template*`.  

You can run the following to see available templates reported by the inventory: 
``` bash
ansible -m debug -a "msg={{ templates }}" -i lab/ labhost
```

!!! warning 
    If there is more than one template reported by the inventory, the lab_vm: 
    has to be told what `template:` to use. 
    
    We will implement a way to set a default template, but for now you have to specify if using multiple template images.

## Creating a template

We can prepare a RHEL7 template by running the role `vm_template` as seen in the [Getting Started](Getting-Started/virtual_machines/#create-a-vm-template).  

The `vm-template` role does some interesting things.  
In the end we will kickstart a VM with a RHEL 7 installation. 

First, it downloads the latest Boot ISO from Red Hat CDN.  
This provides the Anaconda installer, but we still need access to a rhel-7-server-rpms for the installation. 
It then creates a temporary network where the installation will happen.  
While this network has access to Internet, it's not possible to install directly from Red Hat CDN since we need to authenticate with product entitlement certificate to access these repos.  

So the vm-template role calls on the vm-template-proxy role, which will temporarily set up a reverse proxy against Red Hat CDN, passing along your RHEL entitlement certs.  

The installation then begins with

### Customize the kickstart

WIP 

## Create a template manually

WIP


