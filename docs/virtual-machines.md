The main objective is to run Virtual Machines inside our lab, on the networks we've defined.  
We create them by calling the module [`lab_vm`](https://gitlab.com/redhatnordics/consulting-lab/blob/master/library/lab_vm.py) in our roles and playbooks.  

The module itself supports the following options and these are the options that you can define inside your `lab/vms` file.  
Examples below shows how we can specify them in the `lab/vms` config file.  
The only **mandatory** options that you have to specify are the `name:` and `network:` as we can supply a default here.  

-----
### name:
`name:` Enter the name of the VM. This *together* with the `domain:` will be the hostname of the VM.  
        The role automatically appends the `domain:` value to the hostname and adds it during sysprep.  
        It will also be added to the labhosts `/etc/hosts` so we can perform lookups against the hosts. More on this in the Module section below. 

``` yaml
# lab/vms

- name: vm1
  network: mgmt
```

Above example defined the `vm1` vm. Whatever value for `domain:` we've defined in `lab/env` will be appended. So when you connect to the VM you will use the FQDN. eg. `ssh vm1.mylabcomain.org`. 

-----
### network:

`network:` Enter the network that the VM should be placed on.  
           This network has to *exist* by the time we run the `lab_vm` module for obvious reasons, but it's important that you specify a network that is defined in your `lab/networks` file.  
           The precheck will otherwise complain that there is no network matching.  
           You enter the same network name that you've defined in your `lab/networks` file. 

``` yaml
# lab/networks

- name: mgmt
  subnet: 10.50.0.0/24
```
``` yaml
# lab/vms
- name: vm1
  network: mgmt
```

-----
### ip_address:
`ip_address:` Enter a valid ipv4 address that fits inside the range of the network specified.  
              This will ensure that the VM will be given this address when it comes up.

``` yaml
# lab/vms

- name: vm1
  network: mgmt
  ip_address: 10.50.0.40
``` 
-----
### cpu:
`cpu:` How many CPU's should the VM be given.

``` yaml
- name: vm1
  network: mgmt
  cpu: 1
```
----
### memory:
`memory` How much RAM should be given to the VM. 
         Unit is in MB's, should be entered without it.

``` yaml
- name: vm1
  network: mgmt
  cpu: 1
  memory: 2048
```
----
### disk:
`disk:` How big should the primary disk be. Unit is fixed to GB.
        Add the unit to this integer (yeah, sigh).

``` yaml
- name: vm1
  network: mgmt
  cpu: 1
  memory: 2048
  disk: 20G
```
----
### extra_disks:
`extra_disks:` Add and attach additional disks to the VM during creation. 

``` yaml
# lab/vms

- name: vm1
  network: mgmt
  ip_address: 10.50.0.40
  memory: 2048
  cpu: 2
  disk: 20G
  extra_disks:
  - size: 20G
  - size: 50G
    name: userdata
```
----
### port_openings:
If we are using _restricted_ networks we will likely have to perform some port openings.  
We can do this on the VM object **aswell**. 

``` yaml
# lab/vms

 - name: webserver
   network: appnet
   ip_address: 10.1.1.20
   memory: 2048
   cpu: 8
   port_openings:
   - port: 443
     proto: tcp
     from: 10.50.0.0/24
```
----

We most likely will want more than one VM. 
Keep adding VM's as list items like this.  
Once we run the `create-inventory-vms` it will create them one by one.

``` yaml
# lab/vms

- name: vm1
  network: mgmt
  ip_address: 10.50.0.40
  memory: 2048
  cpu: 2
  disk: 20G
  extra_disks:
  - size: 20G
  - size: 50G
    name: userdata

- name: vm2
  network: mgmt
  ip_address: 10.50.0.50
  memory: 2048
  cpu: 2
  disk: 20G

- name: vm3
  network: appnet
  memory: 4096
```

## Role: create-inventory-vms

The [`create-inventory-vms`](https://gitlab.com/redhatnordics/consulting-lab/tree/master/roles/create-inventory-vms) role will iterate each VM definition found in the `lab/vms` config file and run the `lab_vm` module.  
For every iteration it will first recognize if the VM already exists. If it does, not much will happen and will quickly move on to the next.  
This is the behaviour of the `lab_vm:` module itself.  

This enables you to add new VM's as you need them and you simply rerun the role when there is a new VM to be created.

!!! note 
    You do however have the possibility to rerun the role (and in effect the module) to append additional disks.  
    That is, if you've appended an additional disk in `extra_disks:` the role will recognize that and add an additional disk to the VM. 

Once all the vm's are created, the role will check if you are able to connect to the VM.  
It does this using the `ping:` module.  
The `lab_vm:` module will after creation append the host information to Ansible in-memory inventory, much like the [`add_host:`](https://docs.ansible.com/ansible/latest/modules/add_host_module.html) module, which enables us to work on the VM within the same playbook.
This is poweful because it means we can run a single playbook that can automate the whole setup end-to-end if we want to.  
----
You can invoke the `create-inventory-vms` by including it in your own playbook or simply by running the [`create-vms.yaml`](https://gitlab.com/redhatnordics/consulting-lab/blob/master/create-vms.yaml) playbook.


## Module: lab_vm

The heavy lifting of creating VM's is done by the `lab_vm:` module.  
You find it in `library/lab_vm.py` and `action_plugins/lab_vm.py`.  
The [`library/lab_vm.py`](https://gitlab.com/redhatnordics/consulting-lab/blob/patch-june/library/lab_vm.py) is the code executed on the **target** (the labhost), while the [`action_plugins/lab_vm.py`](https://gitlab.com/redhatnordics/consulting-lab/blob/patch-june/action_plugins/lab_vm.py) is executed on your workstation/Ansible controller.  The action plugin is executed first (on your workstation).  

The module handles most of the lifecycling events that needs to happen.  
It creates the VM by cloning the [VM template](/templates).  
As described above, you have the option of specifying which vm-template you want to use with `template:`.  

Once the image has been cloned, it will handle the IP assignation.  
If you've specified the `ip_address:` the module will ensure that it's added to libvirt's network definition within the `<dhcp></dhcp` element. This is important so that we don't get any conflicts later if we are using dynamic assignation on some hosts.  
The ip and the hostname are then added together in the labhosts `/etc/hosts`so the labhost always has knowledge of how to reach the VM.  
This is important because we rely on that to be able to connect to the VM from our workstation and between VM's on different networks.  
Because once it's in the labhost `/etc/hosts` the `lab_vm:` module will send `SIGHUP` to all running **dnsmasq** instances on the labhost, which includes all libvirt networks and the VPN. This causes the dnsmasq's to reread /etc/hosts from the labhost. 

The VM is then sysprepped with the ip address added to /etc/sysconfig/network-scripts/ifcfg-eth0.  

If you haven't specified an `ip_address` the same as above will happen, but we pick up the ip after the actual creation.  


## Delete a Virtual Machine

You can quickly remove a *single* VM by using the playbook `adhoc/delete-vm.yaml`.  

It needs an input parameter `name: `.  
This name is the **shortname** of the VM, so no need to specify the domain.  

Example:  
```
$ ansible-playbook adhoc/delete-vm.yaml -e "name=idm1"
```

The playbooks invokes the `lab_vm:` module with `state: absent`. 

## Delete ALL virtual machines

To delete all Virtual Machines in your lab, you can run the playbook `adhoc/delete-all-of-the-vms.yml`.  
The playbook will look at all VM's reported by libvirt on the labhost and delete them one by one. 

There will be a prompt asking if you are really sure.