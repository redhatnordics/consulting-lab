Examples
========
**WIP**

Here you find ready to use examples of how to setup your lab.
Each example is contained within a folder.
The folders contains an inventory (lab/) that have pre-populated networks (lab/networks) and vms (lab/vms).  
You have to fill in the `lab/env` and the `group_vars/all/rhsm` yourself.

Then, go ahead and target the inventory and run the playbook of choice.

```
ansible-playbook -i examples/<example>/lab examples/<example>/playbook.yml
```

