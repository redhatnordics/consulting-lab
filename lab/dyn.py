#!/usr/bin/env python3

import sys
import libvirt
import json
import socket
import yaml
import os
import subprocess
import getpass
import jinja2

'''
 Labhost dynamic inventory
 forked from https://github.com/maztaim/ansible-libvirt-inventory/blob/master/libvirt-inventory.py

 This inventory can be seen as having three jobs:
 - Read in the lab environment (env, networks and vms) and present them as hostvariables to the labhost
 - Create a host list from all the Virtual Machines running on the labhost.
 - Manage a lab specific SSH config file.
   This file ensures that you connect to the labhost either over the WAN address or the VPN gateway address.
   It also ensures that when you connect to your VM's, ssh should either connect directly to VM through VPN
   or try to use the labhost as a jumphost.

 First thing to happen is that the script reads in all variables it can find in the _adjacent_ files:
 - ./env
 - ./networks
 - ./vms
 These are turned into inventory hostvariables for the labhost which are used by the various roles.  

 Before conneting to the labhost for a VM list, it puts the ~/.ssh/consulting-lab.conf config file in place.  
 We need that for adaptability depending on if you are on VPN or not.  
 It also create a sort of "alias" for labhost. That we can use `ssh labhost` and that will mean either the
 VPN gateway or the WAN address, depending on what is available. 
 
 Next it connects to your labhost for a domain (vm) list.  
 It does so over qemu+ssh://, qemu over ssh.  

 If libvirt is not accessible it either means that we don't have connection
 or that the lab is not fully prepared yet.
 Therefor the Inventory will _NOT_ fail if it can't connect to libvirt, 
 and will instead only present one host; the labhost itself. 

 When something else fails that should stop the execution we use the error() function.  
 It simply wipes all output that has been gathered and replaces it with a inventory_error list.  
 This list is picked up the setup action module, which will print the error more nicely.

'''


# Initialize the dictionary.  May be a horrible way to deal with it, but I am too new to it to make it better.
inventory = {}
inventory['_meta'] = {}
inventory['_meta']['hostvars'] = {}
inventory['all'] = {}
inventory['all']['vars'] = {}
inventory['all']['children'] = ['active', 'inactive', 'ungrouped','labhost']
inventory['active'] = {}
inventory['active']['vars'] = {}
inventory['inactive'] = {}
inventory['inactive']['vars'] = {}
inventory['vms'] = {}
inventory['vms']['vars'] = {}
inventory['labhost'] = {}
inventory['labhost']['hosts'] = ["labhost"]
inventory['labhost']['vars'] = {}
inventory['labhost']['vars']['templates'] = []
inventory['labhost']['vars']['online'] = False


inventory_dir = os.path.dirname(os.path.abspath(__file__))
inventory['all']['vars']['inventory_info'] = []

def print_exit():
    if len(sys.argv) == 2 and sys.argv[1] == '--list':
        print(json.dumps(inventory, indent=4, sort_keys=True))
    elif len(sys.argv) == 3 and sys.argv[1] == '--host':
        print(json.dumps({'ansible_connection': 'ssh'}))
    else:
        sys.stderr.write("Need an argument, either --list or --host <host>\n")
    exit()

def error(error_message):
    # zero the inventory so only the error is  there
    inventory['all']['vars'] = {}
    inventory['labhost']['vars'] = {}
    inventory['all']['vars']['inventory_error'] = str(error_message)
    print_exit()

def info(info_message):
    inventory['all']['vars']['inventory_info'].append(str(info_message))


'''
This is the .ssh/consulting-lab.config template
During each run of the inventory, it will ensure that this file is in place and
included in the  ~/.ssh/config. 

Why?
Instead of making a lot of checks within the inventory for connectivity, 
we can instead make use of a system wide check that adapts if the labhost is reachable
by VPN or WAN or not at all.
It also ensure that we always use StrictHostKeyChecking false etc. which
is really helpful when tearing the labhost up and down.
'''

ssh_config_template = """
# ensures that when we do `ssh labhost` it's run either against the VPN gateway or the WAN.
{% if openvpn_gateway is defined %}
Match exec "/bin/bash -c '[[ '%h' == 'labhost' ]] && timeout 1 nc -z {{ openvpn_gateway }} %p'"
  HostName {{ openvpn_gateway }}
{% endif %}
Match exec "/bin/bash -c '[[ '%h' == 'labhost' ]] && timeout 3 nc -z {{ labhost_wan_address }} %p'"
  Hostname {{ labhost_wan_address }}
Match exec "/bin/bash -c '[[ '%h' == 'labhost' ]]'"
  ProxyCommand bash -c 'printf \"Could not reach labhost on either VPN or WAN\\n\" >&2'

Host labhost
  StrictHostKeyChecking no
  UserKnownHostsFile /dev/null
  {% if inv_priv_key_path is not none %}
  IdentityFile {{ inv_priv_key_path }}
  {% endif %}
  {% if lab_user is not none %}
  User {{ lab_user }}
  {% endif %}
  ServerAliveInterval 60
  ServerAliveCountMax 10
  PasswordAuthentication no


Host {{ labhost_wan_address }}
  StrictHostKeyChecking no
  UserKnownHostsFile /dev/null
  {% if inv_priv_key_path is not none %}
  IdentityFile {{ inv_priv_key_path }}
  {% endif %}
  {% if lab_user is not none %}
  User {{ lab_user }}
  {% endif %}
  ServerAliveInterval 60
  ServerAliveCountMax 10

# Ensures that all ssh connections to a Lab VM 
# will first be tested directly (VPN) and failover
# to jump via the labhost
Host *.{{ domain }}
  ProxyCommand bash -c '(timeout 1 nc -z %h %p) && nc %h %p || printf "direct connection (vpn) not available \\ntrying jump to labhost instead\\n" >&2 && ssh -W %h:%p {{ labhost_wan_address }}'
  {% if inv_priv_key_path is not none %}
  IdentityFile {{ inv_priv_key_path }}
  {% endif %}
  {% if lab_user is not none %}
  User {{ lab_user }}
  {% endif %}
  StrictHostKeyChecking no
  UserKnownHostsFile /dev/null
  ServerAliveInterval 60
  ServerAliveCountMax 10

"""
ssh_config_template = jinja2.Template(ssh_config_template,trim_blocks=True,lstrip_blocks=True)

# Read in the Networks
# If we can't load any networks, we will set an network inventory error
# This will fail the setup module IF we are running network roles
inventory['labhost']['vars']['networks'] = []
network_config_file  = inventory_dir + "/networks"
try:
    with open(network_config_file) as conf:
        # we perform the network check in the setup override
        inventory['labhost']['vars']['networks'] = (yaml.safe_load(conf))
except Exception:
    inventory['labhost']['vars']['inventory_network_error'] = "Couldn't read any networks from %s/networks" % inventory_dir

# Read in the VM's
# If we can't load any VMs, we will set an vm inventory error
# This will fail the setup module IF we are running vm creation/delete roles
inventory['labhost']['vars']['vms'] = []
vms_config_file  = inventory_dir + "/vms"
try:
    with open(vms_config_file) as conf:
        # we perform the network check in the setup override
        inventory['labhost']['vars']['vms'] = (yaml.safe_load(conf))
except Exception:
    inventory['labhost']['vars']['inventory_vms_error'] = "Couldn't read any Virtual Machines from %s/vms" % inventory_dir

# Read the configuration labenv
labenv_config_file = inventory_dir + "/env"
try:
    with open(labenv_config_file) as conf:
        conf = (yaml.safe_load(conf))
        try:
            inventory['all']['vars']['labhost_wan_address'] = conf['labhost_wan_address']
        except KeyError:
            error('Could not find labhost_wan_address in the lab/env file')

        try:
            inventory['all']['vars']['openvpn_gateway'] = conf['openvpn_gateway']
        except KeyError:
            pass
            # Default VPN gateway value is 10.99.99.1
            inventory['all']['vars']['openvpn_gateway'] = "10.99.99.1"

        try: 
            inventory['all']['vars']['domain'] = str(conf['domain'])
        except KeyError as error:
            error("Couldn't find domain in " + labenv_config_file)

        inventory['all']['vars']['user'] = {}
        # If user: is specified use that
        try:
            inventory['all']['vars']['user']['name'] = conf['user']['name']
            lab_user = conf['user']['name']
            
            # user is specified, now we need either a public_key_path or public_key_string
            try:
                inv_pub_key_path = os.path.expanduser(conf['user']['public_key_path'])
            except KeyError:
                inv_pub_key_path = None
            try:
                inv_pub_key_string = conf['user']['public_key_string']
            except KeyError:
                inv_pub_key_string = None

            if inv_pub_key_path is None and inv_pub_key_string is None:
                error("user['name'] is specified in lab/env, but both public_key_path and public_key_string are empty. You have to specify one of them")
            if inv_pub_key_path is not None and inv_pub_key_string is not None:
                error("user['name'] is specified in lab/env, but both public_key_path and public_key_string are specified. You have to use one of them")


            # user: public_key_path: is defined
            if inv_pub_key_path is not None:
                if not os.path.exists(inv_pub_key_path):
                    error("user pub_key_path: %s does not exist or is not accessible" % inv_pub_key_path)
                try:
                    with open(inv_pub_key_path) as f:
                        pk_string = f.readline()
                        if 'ssh-' not in pk_string:
                            error('pub_key_path: %s does not contain a valid ssh- public key' % inv_pub_key_path)

                        inventory['all']['vars']['user']['public_key'] = pk_string.rstrip('\n')
                        inventory['all']['vars']['user']['public_key_path'] = inv_pub_key_path
                except IOError:
                    error('Could not read public_key_path %s: ' % inv_pub_key_path)

            # user: public_key_string: is defined
            else:
                if 'ssh-' not in inv_pub_key_string:
                    error('public_key_string does not contain a valid ssh key')
                inventory['all']['vars']['user']['public_key'] = inv_pub_key_string

            # check if private_key_path: is specified. if so we should include it in the template
            try:
                inv_priv_key_path = os.path.expanduser(conf['user']['private_key_path'])
            except KeyError:
                inv_priv_key_path = None
        except KeyError:
            # otherwise use the system user
            user = getpass.getuser()
            inventory['all']['vars']['user']['name'] = user
            lab_user = user

            homedir = os.environ['HOME']
            ssh_pub_key_path = homedir + "/.ssh/id_rsa.pub"
            inv_priv_key_path = homedir + "/.ssh/id_rsa"
            try:
                with open(ssh_pub_key_path) as pubkey_file:
                    inventory['all']['vars']['user']['public_key'] = str(pubkey_file.readline().strip())
            except IOError:
                sys.stderr.write("Couldn't read public key from " + ssh_pub_key_path )
                error("Couldn't read public key from " + ssh_pub_key_path)

except IOError:
    error('Could not open %s , ensure that here is a env file adjacent to the dynamic inventory' % labenv_config_file)


# Construct the ~/.ssh/consultant-lab.conf
# It's up to the user to include it in their ssh conf
try:
    ssh_lab_conf_file_path = os.getenv('HOME') + "/.ssh/consulting-lab.conf"
    with open(ssh_lab_conf_file_path,"w+") as sshconf:
        sshconf.write(ssh_config_template.render(labhost_wan_address=inventory['all']['vars']['labhost_wan_address'],
                                      openvpn_gateway=inventory['all']['vars']['openvpn_gateway'],
                                      inv_priv_key_path=inv_priv_key_path,
                                      lab_user=lab_user,
                                      domain=inventory['all']['vars']['domain']))
    os.system("chmod 0600 %s" % ssh_lab_conf_file_path)
except IOError as e:
    error('Failed to write to %s: ' + (ssh_lab_conf_file_path,str(e)))

# COMMENTED OUT UNTIL WE DECIDE TO INCLUDE IT 
# adds complexity and we o a bit out of bounds
# by tampering with users SSH config
#
# Check that we Include the consulting lab at the top of the file
# if not append it
#try:
#    ssh_conf_file_path = os.getenv('HOME') + "/.ssh/config"
#    with open(ssh_conf_file_path,"r+") as sshconf_file:
#        sshconf = sshconf_file.read()
#    if "Include consulting-lab.conf" not in sshconf:
#        # TAKE A BACKUP OMG
#        backup_file_path = ssh_conf_file_path + ".orig.bck"
#        # AND DON*T TOUCH THE BACKUP IF IT EXISTS
#        if not os.path.isfile(backup_file_path):
#            os.system("cp %s %s" % (ssh_conf_file_path,backup_file_path))
#        # we have mac users
#        if "darwin" in sys.platform:
#            # check for gsed 
#                # if not blow up
#            os.system("gsed -i '1iInclude consulting-lab.conf' %s" % ssh_conf_file_path )
#        else:
#            os.system("sed -i '1iInclude consulting-lab.conf' %s" % ssh_conf_file_path )
#except IOError as e:
#    error('Could not read ~/.ssh/config: ' + str(e))



# This connection relies on that the user running this inventory OR
# the username and public key specified in the lab/env file.. is
# able to ssh to the labhost without password
#
# However, during first bootrstrap with hetzner
# only root has access! Therefor we check if we are able to ssh as the user with ssh.
# If we can't, then it probably means that the lab is not configured yet, and we should
# not attempt to connect with libvirt
connect_to_labhost_ssh_cmd = ['ssh','labhost','ls']
p = subprocess.Popen(connect_to_labhost_ssh_cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
dhcp_stdout,stderr = p.communicate()
if p.returncode == 0:
    inventory['labhost']['vars']['online'] = True


if inventory['all']['vars']['user']['name'] and inventory['labhost']['vars']['online'] == True:
    host_domain = 'qemu+ssh://%s@labhost/system' % inventory['all']['vars']['user']['name']
    try:
        virthost = libvirt.open(host_domain)
        inventory['labhost']['vars']['online'] = True
    except libvirt.libvirtError as error:
        host_domain = 'qemu+ssh://root@labhost/system'
        try:
            virthost = libvirt.open(host_domain)
            inventory['labhost']['vars']['online'] = True
        except:
            info('Failed to open connection to ' + host_domain + " : " + str(error))
            info('Lab is probably not configured yet')
            inventory['labhost']['vars']['online'] = False


if inventory['labhost']['vars']['online'] == True:
    alldomains = virthost.listAllDomains(0)
    activedomains = virthost.listAllDomains(libvirt.VIR_CONNECT_LIST_DOMAINS_ACTIVE)
    inactivedomains = virthost.listAllDomains(libvirt.VIR_CONNECT_LIST_DOMAINS_INACTIVE)
    
    inventory['active']['hosts'] = [domain.name() for domain in activedomains]
    inventory['vms']['hosts'] = [domain.name() for domain in activedomains]
    inventory['inactive']['hosts'] = [domain.name() for domain in inactivedomains]

    # create a group for the vm short name so we can target them by using the shortname too
    # ie. proxy.my.lab.org  can be targeted with
    # - hosts: proxy
    for domain_name in inventory['vms']['hosts']: 
        shortname = domain_name.split('.')[0]
        inventory[shortname] = {}
        inventory[shortname]['hosts'] = []
        inventory[shortname]['hosts'].append(domain_name)
    
    
    # Take the opportunity here to create a list of templates 
    for vm_name in inventory['inactive']['hosts']:
        if "template" in vm_name:
            inventory['labhost']['vars']['templates'].append(vm_name)
    virthost.close()

print_exit()


