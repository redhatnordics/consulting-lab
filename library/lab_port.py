#!/usr/bin/python
#
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import absolute_import, division, print_function
__metaclass__ = type


ANSIBLE_METADATA = {'metadata_version': '0.0',
                    'status': ['idk'],
                    'supported_by': 'community'}


DOCUMENTATION = r'''
---
module: lab_port
short_description: Create a port opening 
description:
version_added: "0.9"
options:
  network:
  port:
  proto:
  src:
  dst: 
  comment:
notes:
    - This module bypasses the play host loop and only runs once for all the hosts in the play, if you need it
      to iterate use a with\_ directive.
    - Windows targets are supported by this module.
    - The alias 'host' of the parameter 'name' is only available on >=2.4
    - Since Ansible version 2.4, the ``inventory_dir`` variable is now set to ``None`` instead of the 'global inventory source',
      because you can now have multiple sources.  An example was added that shows how to partially restore the previous behaviour.
author:
    - Sebastian Mossberg <smossber@redhat.com>
'''

EXAMPLES = '''


'''
