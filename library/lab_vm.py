#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

ANSIBLE_METADATA = {
    'metadata_version': '0.9',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: lab_vm

short_description: Module to create and destroy VM's within you consulting-lab

version_added: "2.7"

description:
    - Create a RHEL Virtual Machine on your lab.
      The module will clone the template you've created using the vm-template role,
      add it to the network you assign and do all the necessary background stuff:
      - add it to labhost /etc/hosts so it can be picked up by dnsmasq and jumpers.
      - add it to in-memory inventory so it's available in your next play (add_host)
      - add it to your inventory/connections for later connection
      - add static DHCP entrys in libvirt-dnsmasq
      - create backing storage disks.

options:
    name:
        description:
            - Name of the VM. The module will automatically add the `domain:` that you've specified in your inventory vars
        required: true
    cpu:
        description:
            - How many cpu's should be given to the VM (defaults to one)
        required: false
    memory:
        description:
            - How much memory should be given to the VM. No unit (M), just an int. Example memory: 2048
    network:
        description:
            - What libvirt network the host should be added to
        required: true
    ip:
        description:
            - Assign a statically set IP address. If not specified the VM will be given a dynamic ip from libvirt dnsmasq DHCP.
        required: false
    disk:
        description: 
            - How big the primary disk should be.  No unit (G), just an int. Defaults to `20`.  Example `disk: 20`
        required: false
    extra_disks:
        description: 
            - A list of extra disks to create and attach.  `size` needs to be defined No unit (G), just an int. Optional naming of the disk. See example.
        required: false
    port_openings:
        description:
            - A list of dicts with the port openings you want to apply for the VM. The role lab-port-openings will be invoked with the task list host-port-openings. Handled by the action_plugin
    template:
        description:
            - Choose the template you want to use for your VM. If you have more then one template you have to specify this. Otherwise it will default to the single template. 

author:
    Sebastian Mossberg-  (@yourhandle)
'''

EXAMPLES = '''
# Create a Virtual Machine and configure it
- hosts: labhost
  become: true
  vars:
    domain: lab.org
  tasks:
    - name: Create a test vm
      lab-vm:
        name: test-vm
        cpu: 1
        memory: 2048
        network: mgmt

- hosts: test-vm.lab.org
  tasks:
    - name: wait for the vm to come up
      wait_for_connection:

    - name: install something
      yum:
        name: install


# Create a Virtual Machine with a static IP and configure it
- hosts: labhost
  become: true
  vars:
    domain: lab.org
  tasks:
    - name: Create a test vm
      lab-vm:
        name: test-vm
        cpu: 1
        memory: 2048
        network: mgmt
        ip: 10.5.0.5

- hosts: test-vm.lab.org
  tasks:
    - name: wait for the vm to come up
      wait_for_connection:

    - name: install something
      yum:
        name: install

# Create a Virtual Machine with extra disks attached
- hosts: labhost
  become: true
  vars:
    domain: lab.org
  tasks:
    - name: Create a test vm
      lab-vm:
        name: test-vm
        cpu: 1
        memory: 2048
        network: mgmt
        extra_disks:
        - size: 20
          name: data
        - size: 25
        - size: 30
'''

RETURN = '''
original_message:
    description: The original name param that was passed in
    type: str
    returned: always
message:
    description: The output message that the sample module generates
    type: str
    returned: always
'''

from ansible.module_utils.basic import AnsibleModule
import subprocess
import re
import os
import signal
import time

def sighup_dnsmasq():
    ''' 
    In order for dnsmasq to pick up changes, such as new entries in /etc/hosts,
    you send the running process a HUP signal. 
    We do this for _all_ running dnsmasqs. 
    All livirt instances and the VPN dedicated one.
    '''
    # get all dnsmasq pids
    # avoid going outside standard lib with psutil
    dnsmasq_pids = subprocess.check_output(["pidof","dnsmasq"]).split()
    for pid in dnsmasq_pids:
        os.kill(int(pid), signal.SIGHUP)


def create_and_attach_disk(disk,name):
    if "G" not in str(disk['size']):
        disk['size'] = str(disk['size']) + "G"
    # get the current blockdevices
    check_blockdevice_cmd = ["virsh","domblklist",name]
    p = subprocess.Popen(check_blockdevice_cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    check_blockdevice_stdout,stderr = p.communicate()
    if p.returncode != 0:
        module.fail_json(msg="Could not list VM blockdevices. Command was: %s" % check_blockdevice_cmd , exception=stderr)

    # pick out the device identifiers
    check_blockdevice_stdout = check_blockdevice_stdout.splitlines()

    alphabet = ['a','b','c','d','e','f','g']
    new_device_identifier_letter = ""
    # remove the headers from the output [2:]
    for line in filter(None,check_blockdevice_stdout[2:]):
        # awk '{print $1 }'  i.e. vda or sda
        device_identifier        = line.split()[0]
        # 'a' in 'vda'
        device_identifier_letter = device_identifier[2:]
        device_identifier_type   = device_identifier[:2]
        # turn vda into vdb by removing the already taken one from the alphabet list
        if device_identifier_letter in alphabet:
            alphabet.remove(device_identifier_letter)
    new_device_identifier_letter = alphabet[0]
    new_device_identifier = device_identifier_type + new_device_identifier_letter

    # assemble disk name
    try:
        disk_name = "%s_%s_%s" % (name,new_device_identifier,disk['name'])
    except NameError:
        disk_name = "%s_%s" % (name,new_device_identifier)


    # Time to create and attach the extra disk
    create_disk_cmd = ["qemu-img","create","-f","qcow2","/var/lib/libvirt/images/%s.qcow2" % disk_name, disk['size'],"-o","preallocation=metadata"]
    p = subprocess.Popen(create_disk_cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    create_disk_stdout,stderr = p.communicate()
    if p.returncode != 0:
        module.fail_json(msg="Could not create disk. Command was: %s" % create_disk_cmd , exception=stderr)

    attach_disk_cmd = ["virsh","attach-disk",name,"--source","/var/lib/libvirt/images/%s.qcow2" % disk_name,"--target",new_device_identifier,"--persistent","--targetbus","virtio","--driver","qemu","--sourcetype","file","--subdriver","qcow2"]
    p = subprocess.Popen(attach_disk_cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    attach_disk_stdout,stderr = p.communicate()
    if p.returncode != 0:
        module.fail_json(msg="Could not attach disk /var/lib/libvirt/images/%s.qcow2 to %s. Command was: %s" % (disk_name,name,attach_disk_cmd) , exception=stderr)

    # return the action taken
    return "Created and attached %s:/var/lib/libvirt/images/%s.qcow" % (new_device_identifier,disk_name)




'''
There has been multiple times during dev where the statically set ip isn't actually assigned
or "the same" vm is spun up again but with a different ip and it still gets the old.

When running libvirt-dnsmasq, the dnsmasq leasefile is handled by an external --dhcp-script
this script is /usr/libexec/libvirt_leasehelper. This script supports the add,old and init flags.
On startup, dnsmasq runs the script with 'init' and the script returns saved records that it keeps
in /var/lib/libvirt/dnsmasq/<bridge name>.status. This is the same place virsh net-dhcp-leases looks.

However, dnsmasq keeps it's own leases in memory, and will only read in the values there at *startup*.
Removing a record from the persistent lease by either removing the entry from bridge-name.status or
by running the libvirt_leasehelper will NOT remove it from the running dnsmasq. Add to that, whenever
we do SIGHUP on dnsmasq it will add back in all the records it have in memory.

Instead we should go the proper way and tell dnsmasq to remove the record itself.
We do that with the dhcp_release utility from dnsmasq-utils.
'''
def delete_dhcp_lease(network,ip,mac):

        # get the bridge interface of the network
        bridge = ""
        cmd = ['virsh','net-info',network]
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout,stderr = p.communicate()
        if p.returncode != 0:
            raise LookupError("Couldn't get info on network: %s. Error was %s" % (network,stderr))
        for line in stdout.split('\n'):
            if "Bridge:" in line:
                title,bridge = line.strip().split()
        if bridge == '':
            raise LookupError("Couldn't find bridge name for network: %s" % (network))


        remove_cmd = ["dhcp_release",bridge,ip,mac]
        p = subprocess.Popen(remove_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout,stderr = p.communicate()
        if p.returncode != 0:
            raise RuntimeError("Something went wrong deleting the cached DHCP record: %s/ %s on network: %s. The command was %s " % (mac,ip,network, remove_cmd))

        #sighup_dnsmasq()

        # check again if the ip is still there
        #cmd = ['virsh','net-dhcp-leases',network]
        #p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        #stdout,stderr = p.communicate()
        #if p.returncode != 0:
        #        raise LookupError("Couldn't list DHCP leases for network: %s. Error was %s" % (network,stderr))
        #for line in stdout.split("\n"):
        #        if ip + "/" in line:
        #            raise RuntimeError('IP %s still remains in DHCP leases for network "%s" after removal. The removal cmd was: %s. The line found in virsh net-dhcp-leases %s was: %s      Please open an issue about it at https://gitlab.com/redhatnordics/consulting-lab/' % (ip,network,str(remove_cmd),network,str(line)))

        return

def delete_dhcp_lease_with_ip(network,ip):
        cmd = ['virsh','net-dhcp-leases',network]
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout,stderr = p.communicate()
        if p.returncode != 0:
                raise LookupError("Couldn't list DHCP leases for network: %s. Error was %s" % (network,stderr))

        # the mac assigned to the ip
        mac     = ""
        ip_line = ""
        for line in stdout.split("\n"):
                # + "/" is where the prefix start. Otherwise 10.0.0.2 might match 10.0.0.20
                if ip + "/" in line:
                        ip_line = line

        if ip_line == "":
                # IP is not in the list, nothing to do
                return
        else:
                # get mac of
                try:
                    mac = re.findall(r'[0-9a-fA-F]{2}(?::[0-9a-fA-F]{2}){5}', ip_line)[0]
                except IndexError:
                    raise LookupError('Error grepping the MAC from the line %s' % ip_line)
        
        return delete_dhcp_lease(network,ip,mac)

def delete_dhcp_lease_with_mac(network,mac):
        if not re.match(r'[0-9a-fA-F]{2}(?::[0-9a-fA-F]{2}){5}', mac):
            raise RuntimeError("Invalid MAC.")

        # get the lease line
        cmd = ['virsh','net-dhcp-leases',network]
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout,stderr = p.communicate()
        if p.returncode != 0:
                raise LookupError("Couldn't list DHCP leases for network: %s. Error was %s" % (network,stderr))

        # the mac assigned to the ip
        ip     = ""
        lease_line = ""
        for line in stdout.split("\n"):
                if mac in line:
                        lease_line = line

        if lease_line == "":
                # MAC is not in the list, nothing to do
                return
        else:
                # get mac of
                try:
                    ip = re.findall(r'[0-9]+(?:\.[0-9]+){3}', lease_line)[0]
                except IndexError:
                    raise LookupError('Error grepping the IP from the line %s' % lease_line)
        
        return delete_dhcp_lease(network,ip,mac)


def main():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        name=dict(type='str', required=True),
        cpu=dict(type='str', required=False, default="1"),
        memory=dict(type='str', required=False, default="1048"),
        network=dict(type='str', required=False),
        state=dict(type='str', required=False, default="present", choices=['absent', 'present']),
        template=dict(type='str', required=False),
        ip=dict(type='str', required=False, default=None),
        disk=dict(type='str', required=False, default="20G"),
        extra_disks=dict(type='list', required=False, default=None),
        port_openings=dict(type='list', required=False, default=None)
    )


    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )
    name     = module.params['name']
    cpu      = module.params['cpu']
    memory   = module.params['memory']
    network  = module.params['network']
    template = module.params['template']
    disk     = module.params['disk']
    extra_disks  = module.params['extra_disks']
    state    = module.params['state']
    ip       = module.params['ip']
    if ip is None:
        ip   = ""
    if memory == None or memory == "":
        memory = "1024"
    if cpu == None or cpu == "":
        cpu = "1"
    mac = ""      
    if disk is None or disk ==  "":
        disk = "20G"

    if "G" not in disk:
        disk = disk + "G"

    # seed the result dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        message='',
        name=name,
        state=state,
    )
    result['warnings'] = []
    result['actions']  = []

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        module.exit_json(**result)

    # begin by creating a list for existing vm names
    p = subprocess.Popen(['/usr/bin/virsh','list','--all','--name'] ,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout,stderr = p.communicate()
    if p.returncode != 0:
        module.fail_json(msg="Could not list VM's" , exception=stderr)
    vm_list = stdout.split('\n')

    # also create a list of available networks
    p = subprocess.Popen(['/usr/bin/virsh','net-list','--all','--name'] ,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout,stderr = p.communicate()
    if p.returncode != 0:
        module.fail_json(msg="Could not list networks" , exception=stderr)
    net_list = stdout.split('\n')

    vm_already_exists = False
    if name in vm_list:
            #result['message'] = "Already present"
            #module.exit_json(**result)
       vm_already_exists = True


    state = module.params['state']
    # create vm
    if state == 'present':

        # vm is not present, create
        if vm_already_exists:
            result['changed'] = False

        # check that network exist
        if network not in net_list:
            module.fail_json(msg="Network '" + network + "' is not available." , exception=stderr)

        # check that template exist
        if not os.path.isfile('/var/lib/libvirt/images/templates/' + template + '.qcow2'):
            # it didn't, check if there are any available with template-* prefix
            # TODO
            module.fail_json(msg="Template '" + template + "' is not available at /var/lib/libvirt/images/templates/" + template + ".qcow2" , exception=stderr)

        if not vm_already_exists:
            # clone template disk
            clone_cmd = ['qemu-img','create','-f','qcow2',
                         '-b','/var/lib/libvirt/images/templates/' + template + '.qcow2',
                         '/var/lib/libvirt/images/' + name + '.qcow2', disk
                         ] 
            p = subprocess.Popen(clone_cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            stdout,stderr = p.communicate()
            if p.returncode != 0:
                module.fail_json(msg="Could not clone template! The command was %s" % clone_cmd , exception=stderr)
    
            # Create a XML VM definition
            vm_xml_cmd = ['virt-install',
                          '--name',name,
                          '--vcpu',cpu,
                          '--memory',memory,
                          '--disk','/var/lib/libvirt/images/'+name+'.qcow2,bus=virtio',
                          '--import',
                          '--graphics','spice,listen=127.0.0.1',
                          '--noautoconsole',
                          '--network','network='+network+',model=virtio',
                          '--os-variant','linux',
                          '--os-type','rhel7',
                          '--print-xml']
            p = subprocess.Popen(vm_xml_cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            vm_xml_stdout,stderr = p.communicate()
            if p.returncode != 0:
                module.fail_json(msg="Could not create VM" , exception=stderr)
    
            # print the VM xml definition to /tmp
            vm_xml_path = '/tmp/vm-create-%s' % name
            with open(vm_xml_path,'w') as vm_xml_file:
                vm_xml_file.write(vm_xml_stdout)
    
            # get mac from the file definition
            mac = re.findall(r'[0-9a-fA-F]{2}(?::[0-9a-fA-F]{2}){5}', vm_xml_stdout)[0]
    
            ''' 
            # If we have a predefined/static ip eg. `ip: 192.168.122.7` 
            # we need to prepare the DHCP record for it before we create it
            # Otherwise, if we wait until after the VM creation then libvirt dnsmasq might pick it up during boot
            # and create a lease record for it with the wrong ip
            '''
            if ip != '':
            # start by ensuring any dhcp lease for ip is removed
                try:
                    delete_dhcp_lease_with_ip(network,ip)
                except (RuntimeError,LookupError) as error:
                    module.fail_json(msg=error) 
    
    
                dhcp_add_cmd = ["virsh","net-update",network,"add","ip-dhcp-host","<host mac='%s' name='%s' ip='%s'/>" % (mac,name,ip) , "--live","--config"]
                p = subprocess.Popen(dhcp_add_cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
                dhcp_stdout,stderr = p.communicate()
                if p.returncode != 0:
                    module.fail_json(msg="Something went wrong adding the DHCP lease. Command was: %s" % str(dhcp_add_cmd) , exception=stderr)
                sighup_dnsmasq()
    
            # VM Creation
            # Now we define the VM
            vm_create_cmd = ['virsh','define','--file',vm_xml_path]
            p = subprocess.Popen(vm_create_cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            vm_create_stdout,stderr = p.communicate()
            if p.returncode != 0:
                module.fail_json(msg="Could not create VM. Command was: %s" % vm_create_cmd , exception=stderr)

            # virt sysprep
            sysprep_cmd = ['virt-sysprep','-d',name,'--hostname',name,'-operations','defaults,-user-account,-ssh-userdir,-ssh-hostkeys']
            p = subprocess.Popen(sysprep_cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            sysprep_stdout,stderr = p.communicate()
            if p.returncode != 0:
                module.fail_json(msg="Could not sysprep VM. Command was: %s" % sysprep_cmd , exception=stderr)


        '''
        Create additional disks if specified
        extra_disks:
          - size: 20 (required)
            name: data (optional)
            
        the tricky part comes when we want to be able to add disks after the vm has been created, and
        without creating the "same" disk again while and still allowing nameless disks. 
        For now, knowing the no. disk from before and add the remaining will need to suffice
        '''
        if extra_disks != None:
            # first check the no. of already attached disks
            check_blockdevice_cmd = ["virsh","domblklist",name]
            p = subprocess.Popen(check_blockdevice_cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            check_blockdevice_stdout,stderr = p.communicate()
            if p.returncode != 0:
                module.fail_json(msg="Could not list VM blockdevices. Command was: %s" % check_blockdevice_cmd , exception=stderr)

            # remove the 2 lined header and any trailing newlines
            no_disks = len(filter(None,check_blockdevice_stdout.splitlines()[2:]))
            no_extra_disks = len(extra_disks)
            # if number of extra_drives + the initial disk adds up to the same amount of the present disks
            # we don't wanna take any action
            # if no_extra_drives + the initial disk adds up to more than the no. present diks
            # .. that means we should add another disk
            if no_extra_disks + 1 > no_disks:
                # only create the 'new' disk
                no_disks_to_create = int(no_extra_disks) + 1 - int(no_disks)
                # remove the already present disks from the list by reversing
                extra_disks.reverse()
                extra_disks = extra_disks[:no_disks_to_create]
                extra_disks.reverse()

                for disk in extra_disks:
                    result['actions'].append(create_and_attach_disk(disk,name))
                    result['changed'] = True



        if not vm_already_exists:
            # and here we start it..
            vm_start_cmd = ['virsh','start',name]
            p = subprocess.Popen(vm_start_cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            vm_create_stdout,stderr = p.communicate()
            if p.returncode != 0:
                module.fail_json(msg="Could not create VM. Command was: %s" % vm_create_cmd , exception=stderr)
    
    
            '''
            # 
            # if no ip: is given we let libvirt assign it an ip address
            # we then take that ip and add it permanently so we always can be sure it's the same ip
            # and we will add it to /etc/hosts on the labhost
            # 
            '''
            if ip == '':
                dhcp_lookup_cmd = ['virsh','domifaddr',name,'--full','--source','lease']
    
                tries = 0
                loop = True
                while True:
                    p = subprocess.Popen(dhcp_lookup_cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
                    dhcp_stdout,stderr = p.communicate()
                    if p.returncode != 0:
                        module.fail_json(msg="Something went wrong looking up DHCP lease" , exception=stderr)
      
                    # Pick out IP from stdout
                    try:
                        ip = re.findall( r'[0-9]+(?:\.[0-9]+){3}', dhcp_stdout )[0]
                        if ip is None:
                            raise IndexError
                        else:
                            break
                    except IndexError:
                        # ip hasn't appeared yet
                        # increment try count
                        ++tries
                        time.sleep(1)
                    if tries == 20:
                        break
                if ip == '':
                    module.fail_json(msg='Could not find DHCP lease for %s' % name)
    
                # now add it permanently
                dhcp_add_cmd = ["virsh","net-update",network,"add","ip-dhcp-host","<host mac='%s' name='%s' ip='%s'/>" % (mac,name,ip) , "--live","--config"]
                p = subprocess.Popen(dhcp_add_cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
                dhcp_stdout,stderr = p.communicate()
                if p.returncode != 0:
                    module.fail_json(msg="Something went wrong adding the DHCP lease. Command was: %s" % str(dhcp_add_cmd) , exception=stderr)
    
    
            # TODO: the sysprep. different network settings
            # or we skip this completely and just go with the DHCP?
            # if 
    
    
            # add to /etc/hosts
            with open('/etc/hosts','a') as etc_host_file:
                etc_host_file.write("%s %s \n" % (ip,name))
    
            # TODO: send SIGHUP to all runnings dnsmasq instances
            # so VPN clients and other VM's will know about it
            # In the future when we have IdM support, we gotta jack in here somehow
            sighup_dnsmasq()
    
    
            result['changed'] = True
        module.exit_json(**result)


    # absent, remove VM
    else:
        '''
        Absent should ensure a couple of things
        - the vm is undefined and removed from libvirt
        - there should be no DHCP record in the network definition that matches with the name of the vm
        - that the vm is gone from /etc/hosts
        - that the extra disk images are gone
        These should happen in the reverse order from the creation
        '''
        # if the vm is already gone from libvirt
        # it's not much for us to do more than ensure that the host doesn't exist in /etc/hosts
        lines = []
        with open('/etc/hosts','r') as etc_host_file:
            lines = etc_host_file.readlines()
        with open('/etc/hosts','w') as etc_host_file:
            for line in lines:
                if name not in line:
                    etc_host_file.write(line)
                if name in line:
                    result['changed'] = True
                    result['actions'].append("removed %s from /etc/hosts" % name)
        if not vm_already_exists and network is None:
            result['warnings'].append("The VM is gone from libvirt and no network was specified. Specify network: for network clean up actions")
            module.exit_json(**result)


        # if network is not defined, we have to figure it out
        if  (network is None or network == "") and vm_already_exists:
            find_net_cmd= ['virsh','domiflist','--domain',name]
            p = subprocess.Popen(find_net_cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            find_net_stdout,stderr = p.communicate()
            if p.returncode != 0:
                module.fail_json(msg="Something went wrong looking up network for VM %s" % name  , exception=stderr)
            try:
                # third line, third column
                network = find_net_stdout.splitlines()[2].split()[2]
                if network == "":
                    raise LookupError("network came back empty after: " + find_net_cmd)
            except Exception as error:
                module.fail_json(msg="Network not specified and something went wrong looking up network for %s" % name  , exception=error)

        # Check if network actually exist
        network_exists = True
        if network not in net_list:
            network_exists = False
            result['warnings'].append("Network '" + network + "' doesn't exist. No DHCP records can be purged")

        # Attempt to remove any permanent DHCP record for the VM
        if network_exists and network != "":
            # check if there is a permanent DHCP record for the network
            # by simply "grepping" for the name inside the network XML
            netdump_cmd= ['virsh','net-dumpxml',network]
            p = subprocess.Popen(netdump_cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            netdump_stdout,stderr = p.communicate()
            if p.returncode != 0:
                module.fail_json(msg="Something went wrong dumping XML for network %s. Command was %s" % (network,netdump_cmd)  , exception=stderr)

                #dhcp_lookup_cmd = ['virsh','domifaddr',name,'--full','--source','lease']
                #p = subprocess.Popen(dhcp_lookup_cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
                #dhcp_stdout,stderr = p.communicate()
                #if p.returncode != 0:
                #    module.fail_json(msg="Something went wrong looking up DHCP lease" , exception=stderr)

                # Pick out IP from stdout
    
            if name in netdump_stdout:
                # take action on it
                # should really only be one.. but idk
                dhcp_lines_to_remove = []
                lines = []
                for line in netdump_stdout.splitlines():
                    lines.append(line)
                    # get the xml line that contains the dhcp record
                    if name in line:
                        # remove leading and trailing whitespace
                        dhcp_lines_to_remove.append(line.strip())
    
                for xml_line in dhcp_lines_to_remove:
                    try:
                        ip = re.findall( r'[0-9]+(?:\.[0-9]+){3}', xml_line )[0]
                        if ip is None:
                            raise IndexError
                    except IndexError:
                        result['warnings'].append('Could not find IP in static DHCP lease for %s  during removal. ' % name)

                    dhcp_delete_cmd = ["virsh","net-update",network,"delete","ip-dhcp-host",xml_line,"--live","--config"]
                    p = subprocess.Popen(dhcp_delete_cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
                    dhcp_stdout,stderr = p.communicate()
                    if p.returncode != 0:
                        module.fail_json(msg="Could not delete DHCP lease. Command was %s" % dhcp_delete_cmd, exception=stderr )
                        result['warnings'].append("Could not delete DHCP lease matching ip: %s and mac %s" % (ip,mac))
                    result['actions'].append("removed %s from %s network dhcp definition" % (name,network))
                    result['changed'] = True
    
                try:
                    delete_dhcp_lease_with_ip(network,ip)
                    result['changed'] = True
                except (RuntimeError,LookupError) as error:
                    module.fail_json(msg=error) 
            



        
        # vm is present in libvirt, remove
        if name in vm_list:
            remove_cmd = ['virsh','destroy',name]
            p = subprocess.Popen(remove_cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            stdout,stderr = p.communicate()
            #if p.returncode != 0:
                #module.fail_json(msg="Could not destroy VM" , exception=stderr)

            # delete all attached disks inbetween shutting down and undefining 
            check_blockdevice_cmd = ["virsh","domblklist",name]
            p = subprocess.Popen(check_blockdevice_cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            check_blockdevice_stdout,stderr = p.communicate()
            if p.returncode != 0:
                module.fail_json(msg="Could not list VM blockdevices. Command was: %s" % check_blockdevice_cmd , exception=stderr)
            # pick out the device identifiers
            check_blockdevice_stdout = check_blockdevice_stdout.splitlines()
            
            # remove the headers from the output [2:]
            for line in filter(None,check_blockdevice_stdout[2:]):
                # awk '{print $1 }'  get the path to the image file
                disk_path = line.split()[1]
                try:
                    os.remove(disk_path)
                    result['actions'].append("removed disk image %s " % disk_path)
                    result['changed'] = True
                except:
                    raise

            undefine_cmd = ['virsh','undefine',name]
            p = subprocess.Popen(undefine_cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            stdout,stderr = p.communicate()
            if p.returncode != 0:
                module.fail_json(msg="Could not undefine VM" , exception=stderr)

            result['actions'].append("removed %s VM" % name)
            result['changed'] = True



        # reload all dnsmasq instances
        sighup_dnsmasq()

        # were done with all the removing parts
        result['message'] = "Removed %s" % name


    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


if __name__ == '__main__':
    main()
