Role Name
=========

This role puts a Hetzner host into rescue mode and reboots into it
using the Hetzner API. 

Requirements
------------

For this role to work you need a Hetzner Web Service/app user. It can be created in the web interface under Administration -> Settings -> Webservice and app settings.


Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.
|variable|defaults|comments|
|--------|--------|--------|
|hetzner_key_name| |The name of the key when putting it at Hetzner|
|hetzner_key_data| |The public part of the key used to provide a known key to connect to the system in rescue mode.|
|hetzner_password| |The password of the Web Servie/app user|
|hetzner_user| |The name of the Web Service/app user|
|server_ip| |The IP address for the Hetzner machine. It is used as a key to identify the server in the API.|


Dependencies
------------

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------

Created by Mattias Winther (mwinther@redhat.com).
