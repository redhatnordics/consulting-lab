# Hetzner Post Provision

After the hetzner host has been provisioned, this role will perform Hetzner specific tasks like
adding additional non-disks to the volume group and clean up oddities that comes with the vanilla image.  

