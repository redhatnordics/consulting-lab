# Lab Basics

Wrapper role for all other lab- roles that are needed in order to get a _minimal_ Labhost prepared.
Does not include roles such as openvpn and knockd, templates etc.
Instead it will have dependencies to the following roles, in the following order:

- lab-prep
- subman
- lab-libvirt
- lab-iptables

Together they perform the following:
- Ensures that the "lab-user" is present (lab-user role)
- Removes firewalld
- Adds some default firewall rules with iptables
- Installs and configures libvirt (libvirt role)
- subscribes the labhost to Red Hat (subman)
