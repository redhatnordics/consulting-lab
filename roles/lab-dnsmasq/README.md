Lab dnsmasq
===========

In order for VPN clients to connect to VM's they need a DNS service  
providing the VM's hostnames.   
This is described in [connecting-to-vms](/docs/connecting-to-vms.md).  
Therefor we run a dnsmasq on the labhost itself, where VPN clients will send their DNS queries to.  
dnsmasq picks up the /etc/hosts file on the labhost, where we are adding all VM's we create using [vm-create](/roles/vm-create).  

The lab dnsmasq complements the instances created automatically with libvirt. 
For every virtual network we create libvirt runs a dnsmasq.

```
  $ systemctl status 
    ----
             ├─libvirtd.service
             │ ├─26360 /usr/sbin/libvirtd
             │ ├─26519 /usr/sbin/dnsmasq --conf-file=/var/lib/libvirt/dnsmasq/mgmt.conf --leasefile-ro --dhcp-script=/usr/libexec/libvirt_leaseshelper
             │ ├─26520 /usr/sbin/dnsmasq --conf-file=/var/lib/libvirt/dnsmasq/mgmt.conf --leasefile-ro --dhcp-script=/usr/libexec/libvirt_leaseshelper
             │ ├─26581 /usr/sbin/dnsmasq --conf-file=/var/lib/libvirt/dnsmasq/openshift.conf --leasefile-ro --dhcp-script=/usr/libexec/libvirt_leaseshelper
             │ ├─26582 /usr/sbin/dnsmasq --conf-file=/var/lib/libvirt/dnsmasq/openshift.conf --leasefile-ro --dhcp-script=/usr/libexec/libvirt_leaseshelper
             │ ├─26642 /usr/sbin/dnsmasq --conf-file=/var/lib/libvirt/dnsmasq/nat.conf --leasefile-ro --dhcp-script=/usr/libexec/libvirt_leaseshelper
             │ └─26643 /usr/sbin/dnsmasq --conf-file=/var/lib/libvirt/dnsmasq/nat.conf --leasefile-ro --dhcp-script=/usr/libexec/libvirt_leaseshelper
             ├─rsyslog.service
             │ └─23425 /usr/sbin/rsyslogd -n
```
_Here we can see 3 dnsmasq instances for the 3 networks nat,openshift, mgmt._

Unfortunatly, these 


*This role is somewhat intermediate until you install IdM in your lab*  
*When using the lab-IdM role, it will perform a similar setup as this and point your virtual networks to use it for DNS*  
