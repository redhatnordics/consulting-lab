# Lab Prep Labhost

This role will perform some small preperations:
- Make sure the labhost is updated
- Ensures that firewalld is removed early and installs iptables-services
- Ensure that SELinux is enforcing

