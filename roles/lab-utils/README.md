Lab Utils
=========

Role where we put useful scripts and tools.  

In here you currently find:
- sighup-dnsmasq: Small helper script for updating all dnsmasq instances on the labhost.  
                  Used by [vm-create](/roles/vm-create) and [vm-destroy](/roles/vm-destroy).


Add this role as a dependency to your role's meta file if requires a script/setting from here.

```
 $ cat my_role/meta/main.yaml

 dependencies:
   - role: lab-utils
```
