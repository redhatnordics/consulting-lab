Squid
=====

Install and enable an out of the box [Squid] proxy.

Role Variables
--------------

| Variable      | Required | Default | Comments           |
|---------------|----------|---------|--------------------|
| squid_port    | yes      | 3128    | TCP listening port |

Example Playbook
----------------

```yml
- hosts: proxy
  roles:
    - squid
```

```yml
- hosts: proxy
  vars:
    squid_port: 1234
  roles:
    - squid
```

[Squid]: http://www.squid-cache.org
