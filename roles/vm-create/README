Labhost VM-create
=================

Role to create VM's.  
Tightly coupled with [`libvirt_networks`](/roles/libvirt_network/) and [`vm-template`](/roles/vm-template).  
It relies on the `libvirt_networks:` for network placement and `vm-teplate` to provide a template image to _clone_ from.

The role itself creates a single Virtual Machine. To create multiple VM's you have to iterate it with a VM definition.  

The vm definition looks like this: 

```
vm: 
  name:
  network: (name of the libvirt_network)
  ip_address: (valid ipv4 address in the network range)
  memory: (default 1024)
  cpu: (default 1) 
  disk: (default 20G)
  extra_disks: (optional)
  - size: 25G
    name: data
```

When called with a valid `vm:` dict, it will make a clone of the template created with `vm-template`.  
The VM is created with the `virtio` network driver and `virtio` storage driver. All images are created with qcow2.  

`extra_disks:` creates additional qcow2 images and attaches them the VM, but does not do more than that.  
You will have to add them to the Volume Group yourself.   


The clone should be prepared with the users you have defined with [`users:`](/roles/users) along with the public keys added, so it's possible to configure them further with Ansible.  
But in order to do that we need to have functioning network on the host.  

`network:` will make sure the VM pops up on the libvirt_network you define.  
`ip_address:` will be statically set using `virt-sysprep`. Right now this variable is required. All the networks have DHCP on them, but we don't know what IP it will give it and therefor we can't add the IP to the `inventory/connections` file.. making further bootstrapping impossible since ansible has no way of knowing where to connect.  
This is work in progress however and it will be possible to not specify an address but instead rely completely on DHCP.  

As described with `ip_address:`, directly after the VM creation the role will sysprep the image with a static ip-address in `/etc/sysconfig/network-scripts/ifcfg-eth0`.  
The IP address will then be added to the host entry in the inventory for future connections.  
This happens both in the in-memory inventory using [`add_host`](link to ansible.com) so we can immediatly connect and configure the VM after creation *and* to the static inventory file `inventory/connections` for future connections.  
It's added to the group `[vms]`. This group have the special variable [`ansible_ssh_common_args`] overridden with a `ProxyCommand` so that every connection going to VM's will jump through the labhost. 
_We can anticipate issues with this and might require some tuning. When connected over VPN it might be completely unnecessary._ 


Usage
=====

Once vm-template has been run and a template is available you can use this role to create copies.
```
# These roles must've completed _once_ before running the vm-create
- hosts: labhost
  roles:
  - libvirt
  - libvirt-network
  - libvirt-iptables
  - vm-template

- hosts: labhost 
  roles:
  - role: vm-create
    vars:
      name: vm1
      network: testnet
      memory: 2048
      disk: 50G
    
```

Provision and configure right away: 
```
- hosts: labhost 
  roles:
  - role: vm-create
    vars:
      name: vm1
      network: testnet
      memory: 2048
      disk: 50G
  - role: vm-create
    vars:
      name: vm2
      network: testnet
      memory: 2048
      disk: 50G

- hosts: vms
  roles:
  - users
  - motd

- hosts: vm1
  roles:
  - samba-server
```

### Iterate:  

To create multiple VM's at the same time you can look at [`create-inventory-vms`](/roles/create-inventory-vms) which reads the inventory for a list of vms.  
The important part is that this roles recieves a valid `vm:` dictionary. 



